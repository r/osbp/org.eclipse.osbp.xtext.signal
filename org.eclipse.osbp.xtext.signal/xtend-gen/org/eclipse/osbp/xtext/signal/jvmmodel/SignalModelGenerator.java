/**
 * Copyright (c) 2011, 2018 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * 
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 * 
 *  Contributors:
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 * 
 *  This copyright notice shows up in the generated Java code
 */
package org.eclipse.osbp.xtext.signal.jvmmodel;

import com.google.common.collect.Iterators;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URI;
import java.net.URL;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.WatchEvent;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Executors;
import java.util.function.Consumer;
import javax.inject.Inject;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.osbp.core.api.persistence.IPersistenceService;
import org.eclipse.osbp.datainterchange.api.IDataInterchange;
import org.eclipse.osbp.preferences.ProductConfiguration;
import org.eclipse.osbp.xtext.addons.EObjectHelper;
import org.eclipse.osbp.xtext.basic.generator.BasicDslGeneratorUtils;
import org.eclipse.osbp.xtext.datainterchange.DataInterchangeGroup;
import org.eclipse.osbp.xtext.datainterchange.common.WorkerThreadRunnable;
import org.eclipse.osbp.xtext.i18n.I18NModelGenerator;
import org.eclipse.osbp.xtext.signal.CronScheduler;
import org.eclipse.osbp.xtext.signal.DailyScheduler;
import org.eclipse.osbp.xtext.signal.DayOfWeekEnum;
import org.eclipse.osbp.xtext.signal.HourlyScheduler;
import org.eclipse.osbp.xtext.signal.MonthlyScheduler;
import org.eclipse.osbp.xtext.signal.SchedulerType;
import org.eclipse.osbp.xtext.signal.SignalDefinition;
import org.eclipse.osbp.xtext.signal.SignalPackage;
import org.eclipse.osbp.xtext.signal.SignalScheduler;
import org.eclipse.osbp.xtext.signal.SignalWatcher;
import org.eclipse.osbp.xtext.signal.WeeklyScheduler;
import org.eclipse.osbp.xtext.signal.common.SignalConstants;
import org.eclipse.osbp.xtext.signal.common.WatcherImpl;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.generator.IFileSystemAccess;
import org.eclipse.xtext.naming.IQualifiedNameProvider;
import org.eclipse.xtext.naming.QualifiedName;
import org.eclipse.xtext.xbase.compiler.GeneratorConfig;
import org.eclipse.xtext.xbase.compiler.ImportManager;
import org.eclipse.xtext.xbase.compiler.output.TreeAppendable;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.Extension;
import org.eclipse.xtext.xbase.lib.IteratorExtensions;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure1;
import org.quartz.JobDetail;
import org.quartz.JobExecutionException;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings("all")
public class SignalModelGenerator extends I18NModelGenerator {
  @Inject
  @Extension
  private BasicDslGeneratorUtils _basicDslGeneratorUtils;
  
  @Inject
  @Extension
  private IQualifiedNameProvider _iQualifiedNameProvider;
  
  @Override
  public TreeAppendable createAppendable(final EObject context, final ImportManager importManager, final GeneratorConfig config) {
    TreeAppendable _xblockexpression = null;
    {
      this.setBuilder(context.eResource());
      this._basicDslGeneratorUtils.addImportFor(this, importManager, this._typeReferenceBuilder, WatcherImpl.class, SignalConstants.class, Paths.class, WorkerThreadRunnable.class, IPersistenceService.class, IDataInterchange.class, 
        Files.class, FileSystems.class, FileOutputStream.class, Logger.class, LoggerFactory.class, HashMap.class, WatchEvent.class, Path.class, URL.class, Executors.class, Map.class, ArrayList.class, ProductConfiguration.class, 
        File.class, FileInputStream.class, Properties.class, StringWriter.class, PrintWriter.class, SchedulerException.class, IOException.class, Date.class, JobDetail.class, JobExecutionException.class, Trigger.class, 
        URI.class, ThreadFactoryBuilder.class, StdSchedulerFactory.class);
      _xblockexpression = super.createAppendable(context, importManager, config);
    }
    return _xblockexpression;
  }
  
  @Override
  public void doGenerate(final Resource input, final IFileSystemAccess fsa) {
    final Procedure1<SignalPackage> _function = (SignalPackage it) -> {
      this.generatePathConfig(fsa, it);
    };
    IteratorExtensions.<SignalPackage>forEach(Iterators.<SignalPackage>filter(EcoreUtil.<Object>getAllContents(EObjectHelper.getSemanticElement(input), false), SignalPackage.class), _function);
    super.doGenerate(input, fsa);
  }
  
  public void generatePathConfig(final IFileSystemAccess access, final SignalPackage pck) {
    try {
      StringConcatenation _builder = new StringConcatenation();
      String _property = System.getProperty("user.home");
      _builder.append(_property);
      _builder.append("/.osbee");
      File dir = new File(_builder.toString());
      boolean _exists = dir.exists();
      boolean _not = (!_exists);
      if (_not) {
        dir.mkdir();
      }
      final HashSet<DataInterchangeGroup> dataGroups = new HashSet<DataInterchangeGroup>();
      final Consumer<SignalDefinition> _function = (SignalDefinition it) -> {
        if ((it instanceof SignalWatcher)) {
          dataGroups.add(((SignalWatcher)it).getInterchangegroup());
        } else {
          if ((it instanceof SignalScheduler)) {
            dataGroups.add(((SignalScheduler)it).getInterchangegroup());
          }
        }
      };
      pck.getSignals().forEach(_function);
      for (final DataInterchangeGroup group : dataGroups) {
        {
          StringConcatenation _builder_1 = new StringConcatenation();
          String _property_1 = System.getProperty("user.home");
          _builder_1.append(_property_1);
          _builder_1.append("/.osbee/");
          String _name = group.getName();
          _builder_1.append(_name);
          _builder_1.append("Config.xml");
          File file = new File(_builder_1.toString());
          file.setWritable(true, false);
          boolean _exists_1 = file.exists();
          if (_exists_1) {
            final Properties properties = new Properties();
            FileInputStream fileInput = new FileInputStream(file);
            properties.loadFromXML(fileInput);
            fileInput.close();
            final Consumer<SignalDefinition> _function_1 = (SignalDefinition it) -> {
              DataInterchangeGroup tempgroup = ((DataInterchangeGroup) null);
              if ((it instanceof SignalWatcher)) {
                tempgroup = ((SignalWatcher)it).getInterchangegroup();
              } else {
                if ((it instanceof SignalScheduler)) {
                  tempgroup = ((SignalScheduler)it).getInterchangegroup();
                }
              }
              if ((group == tempgroup)) {
                if ((it instanceof SignalWatcher)) {
                  boolean _containsKey = properties.containsKey(SignalConstants.MAXPARALLELTHREADCOUNT_NAME);
                  boolean _not_1 = (!_containsKey);
                  if (_not_1) {
                    properties.put(SignalConstants.MAXPARALLELTHREADCOUNT_NAME, Integer.valueOf(SignalConstants.DEFAULT_PARALLEL_THREADCOUNT).toString());
                  }
                  boolean _isHasFileMask = ((SignalWatcher)it).isHasFileMask();
                  if (_isHasFileMask) {
                    QualifiedName _fullyQualifiedName = this._iQualifiedNameProvider.getFullyQualifiedName(it);
                    String _plus = (_fullyQualifiedName + "Watcher");
                    String _plus_1 = (_plus + SignalConstants.FILEMASK);
                    boolean _containsKey_1 = properties.containsKey(_plus_1);
                    boolean _not_2 = (!_containsKey_1);
                    if (_not_2) {
                      QualifiedName _fullyQualifiedName_1 = this._iQualifiedNameProvider.getFullyQualifiedName(it);
                      String _plus_2 = (_fullyQualifiedName_1 + "Watcher");
                      String _plus_3 = (_plus_2 + SignalConstants.FILEMASK);
                      properties.put(_plus_3, ((SignalWatcher)it).getIdentifier());
                    }
                  } else {
                    QualifiedName _fullyQualifiedName_2 = this._iQualifiedNameProvider.getFullyQualifiedName(it);
                    String _plus_4 = (_fullyQualifiedName_2 + "Watcher");
                    String _plus_5 = (_plus_4 + SignalConstants.FILENAME);
                    boolean _containsKey_2 = properties.containsKey(_plus_5);
                    boolean _not_3 = (!_containsKey_2);
                    if (_not_3) {
                      QualifiedName _fullyQualifiedName_3 = this._iQualifiedNameProvider.getFullyQualifiedName(it);
                      String _plus_6 = (_fullyQualifiedName_3 + "Watcher");
                      String _plus_7 = (_plus_6 + SignalConstants.FILENAME);
                      properties.put(_plus_7, ((SignalWatcher)it).getIdentifier());
                    }
                  }
                } else {
                  if ((it instanceof SignalScheduler)) {
                    if (((((SignalScheduler)it).getSchedulertype() instanceof CronScheduler) && (!properties.containsKey(((this._iQualifiedNameProvider.getFullyQualifiedName(it) + "Scheduler") + SignalConstants.CRON_SCHEDULER))))) {
                      QualifiedName _fullyQualifiedName_4 = this._iQualifiedNameProvider.getFullyQualifiedName(it);
                      String _plus_8 = (_fullyQualifiedName_4 + "Scheduler");
                      String _plus_9 = (_plus_8 + SignalConstants.CRON_SCHEDULER);
                      SchedulerType _schedulertype = ((SignalScheduler)it).getSchedulertype();
                      properties.put(_plus_9, ((CronScheduler) _schedulertype).getExpression());
                    } else {
                      if (((((SignalScheduler)it).getSchedulertype() instanceof HourlyScheduler) && (!properties.containsKey(((this._iQualifiedNameProvider.getFullyQualifiedName(it) + "Scheduler") + SignalConstants.HOURLY_SCHEDULER))))) {
                        QualifiedName _fullyQualifiedName_5 = this._iQualifiedNameProvider.getFullyQualifiedName(it);
                        String _plus_10 = (_fullyQualifiedName_5 + "Scheduler");
                        String _plus_11 = (_plus_10 + SignalConstants.HOURLY_SCHEDULER);
                        SchedulerType _schedulertype_1 = ((SignalScheduler)it).getSchedulertype();
                        properties.put(_plus_11, Integer.valueOf(((HourlyScheduler) _schedulertype_1).getMinute()).toString());
                      } else {
                        if (((((SignalScheduler)it).getSchedulertype() instanceof DailyScheduler) && (!properties.containsKey(((this._iQualifiedNameProvider.getFullyQualifiedName(it) + "Scheduler") + SignalConstants.DAILY_SCHEDULER_HOUR))))) {
                          QualifiedName _fullyQualifiedName_6 = this._iQualifiedNameProvider.getFullyQualifiedName(it);
                          String _plus_12 = (_fullyQualifiedName_6 + "Scheduler");
                          String _plus_13 = (_plus_12 + SignalConstants.DAILY_SCHEDULER_HOUR);
                          StringConcatenation _builder_2 = new StringConcatenation();
                          SchedulerType _schedulertype_2 = ((SignalScheduler)it).getSchedulertype();
                          int _hour = ((DailyScheduler) _schedulertype_2).getHour();
                          _builder_2.append(_hour);
                          properties.put(_plus_13, _builder_2.toString());
                        } else {
                          if (((((SignalScheduler)it).getSchedulertype() instanceof DailyScheduler) && (!properties.containsKey(((this._iQualifiedNameProvider.getFullyQualifiedName(it) + "Scheduler") + SignalConstants.DAILY_SCHEDULER_MIN))))) {
                            QualifiedName _fullyQualifiedName_7 = this._iQualifiedNameProvider.getFullyQualifiedName(it);
                            String _plus_14 = (_fullyQualifiedName_7 + "Scheduler");
                            String _plus_15 = (_plus_14 + SignalConstants.DAILY_SCHEDULER_MIN);
                            StringConcatenation _builder_3 = new StringConcatenation();
                            SchedulerType _schedulertype_3 = ((SignalScheduler)it).getSchedulertype();
                            int _minute = ((DailyScheduler) _schedulertype_3).getMinute();
                            _builder_3.append(_minute);
                            properties.put(_plus_15, _builder_3.toString());
                          } else {
                            if (((((SignalScheduler)it).getSchedulertype() instanceof WeeklyScheduler) && (!properties.containsKey(((this._iQualifiedNameProvider.getFullyQualifiedName(it) + "Scheduler") + SignalConstants.WEEKLY_SCHEDULER_DAYOFWEEK))))) {
                              QualifiedName _fullyQualifiedName_8 = this._iQualifiedNameProvider.getFullyQualifiedName(it);
                              String _plus_16 = (_fullyQualifiedName_8 + "Scheduler");
                              String _plus_17 = (_plus_16 + SignalConstants.WEEKLY_SCHEDULER_DAYOFWEEK);
                              StringConcatenation _builder_4 = new StringConcatenation();
                              SchedulerType _schedulertype_4 = ((SignalScheduler)it).getSchedulertype();
                              DayOfWeekEnum _dayofweek = ((WeeklyScheduler) _schedulertype_4).getDayofweek();
                              _builder_4.append(_dayofweek);
                              properties.put(_plus_17, _builder_4.toString());
                            } else {
                              if (((((SignalScheduler)it).getSchedulertype() instanceof WeeklyScheduler) && (!properties.containsKey(((this._iQualifiedNameProvider.getFullyQualifiedName(it) + "Scheduler") + SignalConstants.WEEKLY_SCHEDULER_HOUR))))) {
                                QualifiedName _fullyQualifiedName_9 = this._iQualifiedNameProvider.getFullyQualifiedName(it);
                                String _plus_18 = (_fullyQualifiedName_9 + "Scheduler");
                                String _plus_19 = (_plus_18 + SignalConstants.WEEKLY_SCHEDULER_HOUR);
                                StringConcatenation _builder_5 = new StringConcatenation();
                                SchedulerType _schedulertype_5 = ((SignalScheduler)it).getSchedulertype();
                                int _hour_1 = ((WeeklyScheduler) _schedulertype_5).getHour();
                                _builder_5.append(_hour_1);
                                properties.put(_plus_19, _builder_5.toString());
                              } else {
                                if (((((SignalScheduler)it).getSchedulertype() instanceof WeeklyScheduler) && (!properties.containsKey(((this._iQualifiedNameProvider.getFullyQualifiedName(it) + "Scheduler") + SignalConstants.WEEKLY_SCHEDULER_MIN))))) {
                                  QualifiedName _fullyQualifiedName_10 = this._iQualifiedNameProvider.getFullyQualifiedName(it);
                                  String _plus_20 = (_fullyQualifiedName_10 + "Scheduler");
                                  String _plus_21 = (_plus_20 + SignalConstants.WEEKLY_SCHEDULER_MIN);
                                  StringConcatenation _builder_6 = new StringConcatenation();
                                  SchedulerType _schedulertype_6 = ((SignalScheduler)it).getSchedulertype();
                                  int _minute_1 = ((WeeklyScheduler) _schedulertype_6).getMinute();
                                  _builder_6.append(_minute_1);
                                  properties.put(_plus_21, _builder_6.toString());
                                } else {
                                  if (((((SignalScheduler)it).getSchedulertype() instanceof MonthlyScheduler) && (!properties.containsKey(((this._iQualifiedNameProvider.getFullyQualifiedName(it) + "Scheduler") + SignalConstants.MONTHLY_SCHEDULER_DAYOFMONTH))))) {
                                    QualifiedName _fullyQualifiedName_11 = this._iQualifiedNameProvider.getFullyQualifiedName(it);
                                    String _plus_22 = (_fullyQualifiedName_11 + "Scheduler");
                                    String _plus_23 = (_plus_22 + SignalConstants.MONTHLY_SCHEDULER_DAYOFMONTH);
                                    StringConcatenation _builder_7 = new StringConcatenation();
                                    SchedulerType _schedulertype_7 = ((SignalScheduler)it).getSchedulertype();
                                    int _dayofmonth = ((MonthlyScheduler) _schedulertype_7).getDayofmonth();
                                    _builder_7.append(_dayofmonth);
                                    properties.put(_plus_23, _builder_7.toString());
                                  } else {
                                    if (((((SignalScheduler)it).getSchedulertype() instanceof MonthlyScheduler) && (!properties.containsKey(((this._iQualifiedNameProvider.getFullyQualifiedName(it) + "Scheduler") + SignalConstants.MONTHLY_SCHEDULER_HOUR))))) {
                                      QualifiedName _fullyQualifiedName_12 = this._iQualifiedNameProvider.getFullyQualifiedName(it);
                                      String _plus_24 = (_fullyQualifiedName_12 + "Scheduler");
                                      String _plus_25 = (_plus_24 + SignalConstants.MONTHLY_SCHEDULER_HOUR);
                                      StringConcatenation _builder_8 = new StringConcatenation();
                                      SchedulerType _schedulertype_8 = ((SignalScheduler)it).getSchedulertype();
                                      int _hour_2 = ((MonthlyScheduler) _schedulertype_8).getHour();
                                      _builder_8.append(_hour_2);
                                      properties.put(_plus_25, _builder_8.toString());
                                    } else {
                                      if (((((SignalScheduler)it).getSchedulertype() instanceof MonthlyScheduler) && (!properties.containsKey(((this._iQualifiedNameProvider.getFullyQualifiedName(it) + "Scheduler") + SignalConstants.MONTHLY_SCHEDULER_MIN))))) {
                                        QualifiedName _fullyQualifiedName_13 = this._iQualifiedNameProvider.getFullyQualifiedName(it);
                                        String _plus_26 = (_fullyQualifiedName_13 + "Scheduler");
                                        String _plus_27 = (_plus_26 + SignalConstants.MONTHLY_SCHEDULER_MIN);
                                        StringConcatenation _builder_9 = new StringConcatenation();
                                        SchedulerType _schedulertype_9 = ((SignalScheduler)it).getSchedulertype();
                                        int _minute_2 = ((MonthlyScheduler) _schedulertype_9).getMinute();
                                        _builder_9.append(_minute_2);
                                        properties.put(_plus_27, _builder_9.toString());
                                      }
                                    }
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            };
            pck.getSignals().forEach(_function_1);
            FileOutputStream fileOutput = new FileOutputStream(file);
            properties.storeToXML(fileOutput, "dataInterchange file URLs");
            fileOutput.close();
          }
        }
      }
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
}
