/**
 * Copyright (c) 2011, 2018 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * 
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 * 
 *  Contributors:
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 * generated by Xtext 2.11.0
 */
package org.eclipse.osbp.xtext.signal.scoping;

import com.google.common.base.Objects;
import java.util.ArrayList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.osbp.xtext.datainterchange.DataInterchange;
import org.eclipse.osbp.xtext.datainterchange.DataInterchangeGroup;
import org.eclipse.osbp.xtext.functionlibrarydsl.FunctionLibraryActionGroup;
import org.eclipse.osbp.xtext.functionlibrarydsl.FunctionLibraryExecute;
import org.eclipse.osbp.xtext.signal.SignalActionTypeEnum;
import org.eclipse.osbp.xtext.signal.SignalDSLPackage;
import org.eclipse.osbp.xtext.signal.SignalDatainterchange;
import org.eclipse.osbp.xtext.signal.SignalDefinition;
import org.eclipse.osbp.xtext.signal.SignalFunction;
import org.eclipse.osbp.xtext.signal.SignalScheduler;
import org.eclipse.osbp.xtext.signal.SignalTask;
import org.eclipse.osbp.xtext.signal.SignalWatcher;
import org.eclipse.osbp.xtext.signal.scoping.AbstractSignalDSLScopeProvider;
import org.eclipse.xtext.scoping.IScope;
import org.eclipse.xtext.scoping.Scopes;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;

/**
 * This class contains custom scoping description.
 * 
 * See https://www.eclipse.org/Xtext/documentation/303_runtime_concepts.html#scoping
 * on how and when to use it.
 */
@SuppressWarnings("all")
public class SignalDSLScopeProvider extends AbstractSignalDSLScopeProvider {
  @Override
  public IScope getScope(final EObject context, final EReference reference) {
    IScope _xifexpression = null;
    boolean _equals = Objects.equal(reference, SignalDSLPackage.Literals.SIGNAL_DATAINTERCHANGE__DATA_REF);
    if (_equals) {
      return this.getScope_DataInterchanges(context, reference);
    } else {
      IScope _xifexpression_1 = null;
      boolean _equals_1 = Objects.equal(reference, SignalDSLPackage.Literals.SIGNAL_FUNCTION__SUPPORT_INTERCHANGE);
      if (_equals_1) {
        return this.getScope_AllowedDataInterchangesForSupportedFile(((SignalFunction) context), reference);
      } else {
        IScope _xifexpression_2 = null;
        boolean _equals_2 = Objects.equal(reference, SignalDSLPackage.Literals.SIGNAL_WATCHER__DEFAULT_INTERCHANGE);
        if (_equals_2) {
          return this.getScope_AllowedInterchangesForDefault(((SignalDefinition) context), reference);
        } else {
          IScope _xifexpression_3 = null;
          boolean _equals_3 = Objects.equal(reference, SignalDSLPackage.Literals.SIGNAL_FUNCTION__DO_EXECUTE_FUNCTION);
          if (_equals_3) {
            return this.getScope_DoExecuteFunctions(((SignalFunction) context).getGroup(), reference);
          } else {
            _xifexpression_3 = super.getScope(context, reference);
          }
          _xifexpression_2 = _xifexpression_3;
        }
        _xifexpression_1 = _xifexpression_2;
      }
      _xifexpression = _xifexpression_1;
    }
    return _xifexpression;
  }
  
  public IScope getScope_DataInterchanges(final EObject context, final EReference reference) {
    ArrayList<EObject> result = CollectionLiterals.<EObject>newArrayList();
    if ((context != null)) {
      EObject signal = context.eContainer();
      DataInterchangeGroup group = ((DataInterchangeGroup) null);
      if ((signal instanceof SignalWatcher)) {
        group = ((SignalWatcher)signal).getInterchangegroup();
      } else {
        if ((signal instanceof SignalScheduler)) {
          group = ((SignalScheduler)signal).getInterchangegroup();
        }
      }
      EList<DataInterchange> _datInts = group.getDatInts();
      for (final DataInterchange value : _datInts) {
        result.add(value);
      }
    }
    return Scopes.scopeFor(result);
  }
  
  public IScope getScope_AllowedInterchangesForDefault(final SignalDefinition signal, final EReference reference) {
    ArrayList<EObject> result = CollectionLiterals.<EObject>newArrayList();
    if ((signal != null)) {
      DataInterchangeGroup group = signal.getInterchangegroup();
      EList<DataInterchange> _datInts = group.getDatInts();
      for (final DataInterchange value : _datInts) {
        result.add(value);
      }
      return Scopes.scopeFor(result);
    }
    return null;
  }
  
  public IScope getScope_AllowedDataInterchangesForSupportedFile(final SignalFunction function, final EReference reference) {
    ArrayList<EObject> result = CollectionLiterals.<EObject>newArrayList();
    if ((function != null)) {
      EObject signal = function.eContainer();
      if ((signal instanceof SignalDefinition)) {
        if (((((SignalDefinition)signal).getTasks() != null) && (!((SignalDefinition)signal).getTasks().isEmpty()))) {
          EList<SignalTask> _tasks = ((SignalDefinition)signal).getTasks();
          for (final SignalTask task : _tasks) {
            if ((task instanceof SignalDatainterchange)) {
              if ((function.isOnExportFile() && ((SignalDatainterchange)task).getActionType().equals(SignalActionTypeEnum.DATAEXPORT))) {
                result.add(((SignalDatainterchange)task).getDataRef());
              } else {
                if ((function.isOnImportFile() && ((SignalDatainterchange)task).getActionType().equals(SignalActionTypeEnum.DATAIMPORT))) {
                  result.add(((SignalDatainterchange)task).getDataRef());
                }
              }
            }
          }
        }
      }
    }
    return Scopes.scopeFor(result);
  }
  
  public IScope getScope_AllowedDefaultDataInterchanges(final FunctionLibraryActionGroup group, final EReference reference) {
    ArrayList<EObject> result = CollectionLiterals.<EObject>newArrayList();
    if ((group != null)) {
      EList<FunctionLibraryExecute> _executes = group.getExecutes();
      for (final FunctionLibraryExecute function : _executes) {
        result.add(function);
      }
    }
    return Scopes.scopeFor(result);
  }
  
  public IScope getScope_DoExecuteFunctions(final FunctionLibraryActionGroup group, final EReference reference) {
    ArrayList<EObject> result = CollectionLiterals.<EObject>newArrayList();
    if ((group != null)) {
      EList<FunctionLibraryExecute> _executes = group.getExecutes();
      for (final FunctionLibraryExecute function : _executes) {
        result.add(function);
      }
    }
    return Scopes.scopeFor(result);
  }
}
