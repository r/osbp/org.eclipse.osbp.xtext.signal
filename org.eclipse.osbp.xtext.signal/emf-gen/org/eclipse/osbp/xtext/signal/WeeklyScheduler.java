/**
 * Copyright (c) 2011, 2018 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0 
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:                                                      
 *     Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 *  
 *  generated from SignalDSL.xcore
 * 
 *  
 */
package org.eclipse.osbp.xtext.signal;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Weekly Scheduler</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.signal.WeeklyScheduler#getDayofweek <em>Dayofweek</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.signal.WeeklyScheduler#getHour <em>Hour</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.signal.WeeklyScheduler#getMinute <em>Minute</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.xtext.signal.SignalDSLPackage#getWeeklyScheduler()
 * @model
 * @generated
 */
public interface WeeklyScheduler extends SchedulerType {
	/**
	 * Returns the value of the '<em><b>Dayofweek</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.osbp.xtext.signal.DayOfWeekEnum}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dayofweek</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dayofweek</em>' attribute.
	 * @see org.eclipse.osbp.xtext.signal.DayOfWeekEnum
	 * @see #setDayofweek(DayOfWeekEnum)
	 * @see org.eclipse.osbp.xtext.signal.SignalDSLPackage#getWeeklyScheduler_Dayofweek()
	 * @model unique="false"
	 * @generated
	 */
	DayOfWeekEnum getDayofweek();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.signal.WeeklyScheduler#getDayofweek <em>Dayofweek</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Dayofweek</em>' attribute.
	 * @see org.eclipse.osbp.xtext.signal.DayOfWeekEnum
	 * @see #getDayofweek()
	 * @generated
	 */
	void setDayofweek(DayOfWeekEnum value);

	/**
	 * Returns the value of the '<em><b>Hour</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hour</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hour</em>' attribute.
	 * @see #setHour(int)
	 * @see org.eclipse.osbp.xtext.signal.SignalDSLPackage#getWeeklyScheduler_Hour()
	 * @model unique="false"
	 * @generated
	 */
	int getHour();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.signal.WeeklyScheduler#getHour <em>Hour</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Hour</em>' attribute.
	 * @see #getHour()
	 * @generated
	 */
	void setHour(int value);

	/**
	 * Returns the value of the '<em><b>Minute</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Minute</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Minute</em>' attribute.
	 * @see #setMinute(int)
	 * @see org.eclipse.osbp.xtext.signal.SignalDSLPackage#getWeeklyScheduler_Minute()
	 * @model unique="false"
	 * @generated
	 */
	int getMinute();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.signal.WeeklyScheduler#getMinute <em>Minute</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Minute</em>' attribute.
	 * @see #getMinute()
	 * @generated
	 */
	void setMinute(int value);

} // WeeklyScheduler
