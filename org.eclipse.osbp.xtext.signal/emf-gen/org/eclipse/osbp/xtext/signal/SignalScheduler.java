/**
 * Copyright (c) 2011, 2018 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0 
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:                                                      
 *     Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 *  
 *  generated from SignalDSL.xcore
 * 
 *  
 */
package org.eclipse.osbp.xtext.signal;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Signal Scheduler</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.signal.SignalScheduler#getSchedulertype <em>Schedulertype</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.xtext.signal.SignalDSLPackage#getSignalScheduler()
 * @model
 * @generated
 */
public interface SignalScheduler extends SignalDefinition {
	/**
	 * Returns the value of the '<em><b>Schedulertype</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Schedulertype</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Schedulertype</em>' containment reference.
	 * @see #setSchedulertype(SchedulerType)
	 * @see org.eclipse.osbp.xtext.signal.SignalDSLPackage#getSignalScheduler_Schedulertype()
	 * @model containment="true"
	 * @generated
	 */
	SchedulerType getSchedulertype();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.signal.SignalScheduler#getSchedulertype <em>Schedulertype</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Schedulertype</em>' containment reference.
	 * @see #getSchedulertype()
	 * @generated
	 */
	void setSchedulertype(SchedulerType value);

} // SignalScheduler
