/**
 * Copyright (c) 2011, 2018 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0 
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:                                                      
 *     Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 *  
 *  generated from SignalDSL.xcore
 * 
 *  
 */
package org.eclipse.osbp.xtext.signal;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Cron Scheduler</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.signal.CronScheduler#getExpression <em>Expression</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.xtext.signal.SignalDSLPackage#getCronScheduler()
 * @model
 * @generated
 */
public interface CronScheduler extends SchedulerType {
	/**
	 * Returns the value of the '<em><b>Expression</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Expression</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Expression</em>' attribute.
	 * @see #setExpression(String)
	 * @see org.eclipse.osbp.xtext.signal.SignalDSLPackage#getCronScheduler_Expression()
	 * @model unique="false"
	 * @generated
	 */
	String getExpression();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.signal.CronScheduler#getExpression <em>Expression</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Expression</em>' attribute.
	 * @see #getExpression()
	 * @generated
	 */
	void setExpression(String value);

} // CronScheduler
