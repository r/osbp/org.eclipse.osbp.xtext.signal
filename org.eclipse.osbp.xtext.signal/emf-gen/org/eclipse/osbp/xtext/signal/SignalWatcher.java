/**
 * Copyright (c) 2011, 2018 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0 
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:                                                      
 *     Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 *  
 *  generated from SignalDSL.xcore
 * 
 *  
 */
package org.eclipse.osbp.xtext.signal;

import org.eclipse.osbp.xtext.datainterchange.DataInterchange;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Signal Watcher</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.signal.SignalWatcher#getDefaultInterchange <em>Default Interchange</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.signal.SignalWatcher#isHasFileMask <em>Has File Mask</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.signal.SignalWatcher#getIdentifier <em>Identifier</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.xtext.signal.SignalDSLPackage#getSignalWatcher()
 * @model
 * @generated
 */
public interface SignalWatcher extends SignalDefinition {
	/**
	 * Returns the value of the '<em><b>Default Interchange</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Default Interchange</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Default Interchange</em>' reference.
	 * @see #setDefaultInterchange(DataInterchange)
	 * @see org.eclipse.osbp.xtext.signal.SignalDSLPackage#getSignalWatcher_DefaultInterchange()
	 * @model
	 * @generated
	 */
	DataInterchange getDefaultInterchange();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.signal.SignalWatcher#getDefaultInterchange <em>Default Interchange</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Default Interchange</em>' reference.
	 * @see #getDefaultInterchange()
	 * @generated
	 */
	void setDefaultInterchange(DataInterchange value);

	/**
	 * Returns the value of the '<em><b>Has File Mask</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has File Mask</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has File Mask</em>' attribute.
	 * @see #setHasFileMask(boolean)
	 * @see org.eclipse.osbp.xtext.signal.SignalDSLPackage#getSignalWatcher_HasFileMask()
	 * @model unique="false"
	 * @generated
	 */
	boolean isHasFileMask();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.signal.SignalWatcher#isHasFileMask <em>Has File Mask</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Has File Mask</em>' attribute.
	 * @see #isHasFileMask()
	 * @generated
	 */
	void setHasFileMask(boolean value);

	/**
	 * Returns the value of the '<em><b>Identifier</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Identifier</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Identifier</em>' attribute.
	 * @see #setIdentifier(String)
	 * @see org.eclipse.osbp.xtext.signal.SignalDSLPackage#getSignalWatcher_Identifier()
	 * @model unique="false"
	 * @generated
	 */
	String getIdentifier();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.signal.SignalWatcher#getIdentifier <em>Identifier</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Identifier</em>' attribute.
	 * @see #getIdentifier()
	 * @generated
	 */
	void setIdentifier(String value);

} // SignalWatcher
