/**
 * Copyright (c) 2011, 2018 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0 
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:                                                      
 *     Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 *  
 *  generated from SignalDSL.xcore
 * 
 *  
 */
package org.eclipse.osbp.xtext.signal;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.eclipse.osbp.xtext.signal.SignalDSLPackage
 * @generated
 */
public interface SignalDSLFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	SignalDSLFactory eINSTANCE = org.eclipse.osbp.xtext.signal.impl.SignalDSLFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Signal Model</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Signal Model</em>'.
	 * @generated
	 */
	SignalModel createSignalModel();

	/**
	 * Returns a new object of class '<em>Signal Lazy Resolver</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Signal Lazy Resolver</em>'.
	 * @generated
	 */
	SignalLazyResolver createSignalLazyResolver();

	/**
	 * Returns a new object of class '<em>Signal Package</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Signal Package</em>'.
	 * @generated
	 */
	SignalPackage createSignalPackage();

	/**
	 * Returns a new object of class '<em>Signal Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Signal Definition</em>'.
	 * @generated
	 */
	SignalDefinition createSignalDefinition();

	/**
	 * Returns a new object of class '<em>Signal Watcher</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Signal Watcher</em>'.
	 * @generated
	 */
	SignalWatcher createSignalWatcher();

	/**
	 * Returns a new object of class '<em>Signal Scheduler</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Signal Scheduler</em>'.
	 * @generated
	 */
	SignalScheduler createSignalScheduler();

	/**
	 * Returns a new object of class '<em>Scheduler Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Scheduler Type</em>'.
	 * @generated
	 */
	SchedulerType createSchedulerType();

	/**
	 * Returns a new object of class '<em>Cron Scheduler</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Cron Scheduler</em>'.
	 * @generated
	 */
	CronScheduler createCronScheduler();

	/**
	 * Returns a new object of class '<em>Hourly Scheduler</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Hourly Scheduler</em>'.
	 * @generated
	 */
	HourlyScheduler createHourlyScheduler();

	/**
	 * Returns a new object of class '<em>Daily Scheduler</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Daily Scheduler</em>'.
	 * @generated
	 */
	DailyScheduler createDailyScheduler();

	/**
	 * Returns a new object of class '<em>Weekly Scheduler</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Weekly Scheduler</em>'.
	 * @generated
	 */
	WeeklyScheduler createWeeklyScheduler();

	/**
	 * Returns a new object of class '<em>Monthly Scheduler</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Monthly Scheduler</em>'.
	 * @generated
	 */
	MonthlyScheduler createMonthlyScheduler();

	/**
	 * Returns a new object of class '<em>Signal Task</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Signal Task</em>'.
	 * @generated
	 */
	SignalTask createSignalTask();

	/**
	 * Returns a new object of class '<em>Signal Datainterchange</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Signal Datainterchange</em>'.
	 * @generated
	 */
	SignalDatainterchange createSignalDatainterchange();

	/**
	 * Returns a new object of class '<em>Signal Function</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Signal Function</em>'.
	 * @generated
	 */
	SignalFunction createSignalFunction();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	SignalDSLPackage getSignalDSLPackage();

} //SignalDSLFactory
