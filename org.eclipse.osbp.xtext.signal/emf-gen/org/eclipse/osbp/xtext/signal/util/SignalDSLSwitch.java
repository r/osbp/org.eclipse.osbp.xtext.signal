/**
 * Copyright (c) 2011, 2018 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0 
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:                                                      
 *     Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 *  
 *  generated from SignalDSL.xcore
 * 
 *  
 */
package org.eclipse.osbp.xtext.signal.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

import org.eclipse.osbp.dsl.semantic.common.types.LLazyResolver;
import org.eclipse.osbp.dsl.semantic.common.types.LPackage;

import org.eclipse.osbp.xtext.signal.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see org.eclipse.osbp.xtext.signal.SignalDSLPackage
 * @generated
 */
public class SignalDSLSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static SignalDSLPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SignalDSLSwitch() {
		if (modelPackage == null) {
			modelPackage = SignalDSLPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case SignalDSLPackage.SIGNAL_MODEL: {
				SignalModel signalModel = (SignalModel)theEObject;
				T result = caseSignalModel(signalModel);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SignalDSLPackage.SIGNAL_LAZY_RESOLVER: {
				SignalLazyResolver signalLazyResolver = (SignalLazyResolver)theEObject;
				T result = caseSignalLazyResolver(signalLazyResolver);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SignalDSLPackage.SIGNAL_PACKAGE: {
				SignalPackage signalPackage = (SignalPackage)theEObject;
				T result = caseSignalPackage(signalPackage);
				if (result == null) result = caseLPackage(signalPackage);
				if (result == null) result = caseLLazyResolver(signalPackage);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SignalDSLPackage.SIGNAL_DEFINITION: {
				SignalDefinition signalDefinition = (SignalDefinition)theEObject;
				T result = caseSignalDefinition(signalDefinition);
				if (result == null) result = caseSignalLazyResolver(signalDefinition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SignalDSLPackage.SIGNAL_WATCHER: {
				SignalWatcher signalWatcher = (SignalWatcher)theEObject;
				T result = caseSignalWatcher(signalWatcher);
				if (result == null) result = caseSignalDefinition(signalWatcher);
				if (result == null) result = caseSignalLazyResolver(signalWatcher);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SignalDSLPackage.SIGNAL_SCHEDULER: {
				SignalScheduler signalScheduler = (SignalScheduler)theEObject;
				T result = caseSignalScheduler(signalScheduler);
				if (result == null) result = caseSignalDefinition(signalScheduler);
				if (result == null) result = caseSignalLazyResolver(signalScheduler);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SignalDSLPackage.SCHEDULER_TYPE: {
				SchedulerType schedulerType = (SchedulerType)theEObject;
				T result = caseSchedulerType(schedulerType);
				if (result == null) result = caseSignalLazyResolver(schedulerType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SignalDSLPackage.CRON_SCHEDULER: {
				CronScheduler cronScheduler = (CronScheduler)theEObject;
				T result = caseCronScheduler(cronScheduler);
				if (result == null) result = caseSchedulerType(cronScheduler);
				if (result == null) result = caseSignalLazyResolver(cronScheduler);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SignalDSLPackage.HOURLY_SCHEDULER: {
				HourlyScheduler hourlyScheduler = (HourlyScheduler)theEObject;
				T result = caseHourlyScheduler(hourlyScheduler);
				if (result == null) result = caseSchedulerType(hourlyScheduler);
				if (result == null) result = caseSignalLazyResolver(hourlyScheduler);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SignalDSLPackage.DAILY_SCHEDULER: {
				DailyScheduler dailyScheduler = (DailyScheduler)theEObject;
				T result = caseDailyScheduler(dailyScheduler);
				if (result == null) result = caseSchedulerType(dailyScheduler);
				if (result == null) result = caseSignalLazyResolver(dailyScheduler);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SignalDSLPackage.WEEKLY_SCHEDULER: {
				WeeklyScheduler weeklyScheduler = (WeeklyScheduler)theEObject;
				T result = caseWeeklyScheduler(weeklyScheduler);
				if (result == null) result = caseSchedulerType(weeklyScheduler);
				if (result == null) result = caseSignalLazyResolver(weeklyScheduler);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SignalDSLPackage.MONTHLY_SCHEDULER: {
				MonthlyScheduler monthlyScheduler = (MonthlyScheduler)theEObject;
				T result = caseMonthlyScheduler(monthlyScheduler);
				if (result == null) result = caseSchedulerType(monthlyScheduler);
				if (result == null) result = caseSignalLazyResolver(monthlyScheduler);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SignalDSLPackage.SIGNAL_TASK: {
				SignalTask signalTask = (SignalTask)theEObject;
				T result = caseSignalTask(signalTask);
				if (result == null) result = caseSignalLazyResolver(signalTask);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SignalDSLPackage.SIGNAL_DATAINTERCHANGE: {
				SignalDatainterchange signalDatainterchange = (SignalDatainterchange)theEObject;
				T result = caseSignalDatainterchange(signalDatainterchange);
				if (result == null) result = caseSignalTask(signalDatainterchange);
				if (result == null) result = caseSignalLazyResolver(signalDatainterchange);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SignalDSLPackage.SIGNAL_FUNCTION: {
				SignalFunction signalFunction = (SignalFunction)theEObject;
				T result = caseSignalFunction(signalFunction);
				if (result == null) result = caseSignalTask(signalFunction);
				if (result == null) result = caseSignalLazyResolver(signalFunction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Signal Model</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Signal Model</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSignalModel(SignalModel object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Signal Lazy Resolver</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Signal Lazy Resolver</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSignalLazyResolver(SignalLazyResolver object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Signal Package</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Signal Package</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSignalPackage(SignalPackage object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Signal Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Signal Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSignalDefinition(SignalDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Signal Watcher</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Signal Watcher</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSignalWatcher(SignalWatcher object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Signal Scheduler</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Signal Scheduler</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSignalScheduler(SignalScheduler object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Scheduler Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Scheduler Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSchedulerType(SchedulerType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cron Scheduler</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cron Scheduler</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCronScheduler(CronScheduler object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Hourly Scheduler</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Hourly Scheduler</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHourlyScheduler(HourlyScheduler object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Daily Scheduler</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Daily Scheduler</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDailyScheduler(DailyScheduler object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Weekly Scheduler</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Weekly Scheduler</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseWeeklyScheduler(WeeklyScheduler object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Monthly Scheduler</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Monthly Scheduler</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMonthlyScheduler(MonthlyScheduler object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Signal Task</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Signal Task</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSignalTask(SignalTask object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Signal Datainterchange</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Signal Datainterchange</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSignalDatainterchange(SignalDatainterchange object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Signal Function</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Signal Function</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSignalFunction(SignalFunction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>LLazy Resolver</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>LLazy Resolver</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLLazyResolver(LLazyResolver object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>LPackage</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>LPackage</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLPackage(LPackage object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //SignalDSLSwitch
