/**
 * Copyright (c) 2011, 2018 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0 
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:                                                      
 *     Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 *  
 *  generated from SignalDSL.xcore
 * 
 *  
 */
package org.eclipse.osbp.xtext.signal;

import org.eclipse.osbp.xtext.datainterchange.DataInterchange;

import org.eclipse.osbp.xtext.functionlibrarydsl.FunctionLibraryActionGroup;
import org.eclipse.osbp.xtext.functionlibrarydsl.FunctionLibraryExecute;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Signal Function</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.signal.SignalFunction#getGroup <em>Group</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.signal.SignalFunction#getDoExecuteFunction <em>Do Execute Function</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.signal.SignalFunction#isOnImportFile <em>On Import File</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.signal.SignalFunction#isOnExportFile <em>On Export File</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.signal.SignalFunction#getSupportInterchange <em>Support Interchange</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.xtext.signal.SignalDSLPackage#getSignalFunction()
 * @model
 * @generated
 */
public interface SignalFunction extends SignalTask {
	/**
	 * Returns the value of the '<em><b>Group</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Group</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Group</em>' reference.
	 * @see #setGroup(FunctionLibraryActionGroup)
	 * @see org.eclipse.osbp.xtext.signal.SignalDSLPackage#getSignalFunction_Group()
	 * @model
	 * @generated
	 */
	FunctionLibraryActionGroup getGroup();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.signal.SignalFunction#getGroup <em>Group</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Group</em>' reference.
	 * @see #getGroup()
	 * @generated
	 */
	void setGroup(FunctionLibraryActionGroup value);

	/**
	 * Returns the value of the '<em><b>Do Execute Function</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Do Execute Function</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Do Execute Function</em>' reference.
	 * @see #setDoExecuteFunction(FunctionLibraryExecute)
	 * @see org.eclipse.osbp.xtext.signal.SignalDSLPackage#getSignalFunction_DoExecuteFunction()
	 * @model
	 * @generated
	 */
	FunctionLibraryExecute getDoExecuteFunction();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.signal.SignalFunction#getDoExecuteFunction <em>Do Execute Function</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Do Execute Function</em>' reference.
	 * @see #getDoExecuteFunction()
	 * @generated
	 */
	void setDoExecuteFunction(FunctionLibraryExecute value);

	/**
	 * Returns the value of the '<em><b>On Import File</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>On Import File</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>On Import File</em>' attribute.
	 * @see #setOnImportFile(boolean)
	 * @see org.eclipse.osbp.xtext.signal.SignalDSLPackage#getSignalFunction_OnImportFile()
	 * @model unique="false"
	 * @generated
	 */
	boolean isOnImportFile();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.signal.SignalFunction#isOnImportFile <em>On Import File</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>On Import File</em>' attribute.
	 * @see #isOnImportFile()
	 * @generated
	 */
	void setOnImportFile(boolean value);

	/**
	 * Returns the value of the '<em><b>On Export File</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>On Export File</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>On Export File</em>' attribute.
	 * @see #setOnExportFile(boolean)
	 * @see org.eclipse.osbp.xtext.signal.SignalDSLPackage#getSignalFunction_OnExportFile()
	 * @model unique="false"
	 * @generated
	 */
	boolean isOnExportFile();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.signal.SignalFunction#isOnExportFile <em>On Export File</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>On Export File</em>' attribute.
	 * @see #isOnExportFile()
	 * @generated
	 */
	void setOnExportFile(boolean value);

	/**
	 * Returns the value of the '<em><b>Support Interchange</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Support Interchange</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Support Interchange</em>' reference.
	 * @see #setSupportInterchange(DataInterchange)
	 * @see org.eclipse.osbp.xtext.signal.SignalDSLPackage#getSignalFunction_SupportInterchange()
	 * @model
	 * @generated
	 */
	DataInterchange getSupportInterchange();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.signal.SignalFunction#getSupportInterchange <em>Support Interchange</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Support Interchange</em>' reference.
	 * @see #getSupportInterchange()
	 * @generated
	 */
	void setSupportInterchange(DataInterchange value);

} // SignalFunction
