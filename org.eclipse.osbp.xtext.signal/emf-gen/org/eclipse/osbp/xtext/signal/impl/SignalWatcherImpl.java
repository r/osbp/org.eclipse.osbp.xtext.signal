/**
 * Copyright (c) 2011, 2018 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0 
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:                                                      
 *     Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 *  
 *  generated from SignalDSL.xcore
 * 
 *  
 */
package org.eclipse.osbp.xtext.signal.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.osbp.xtext.datainterchange.DataInterchange;

import org.eclipse.osbp.xtext.signal.SignalDSLPackage;
import org.eclipse.osbp.xtext.signal.SignalWatcher;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Signal Watcher</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.signal.impl.SignalWatcherImpl#getDefaultInterchange <em>Default Interchange</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.signal.impl.SignalWatcherImpl#isHasFileMask <em>Has File Mask</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.signal.impl.SignalWatcherImpl#getIdentifier <em>Identifier</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SignalWatcherImpl extends SignalDefinitionImpl implements SignalWatcher {
	/**
	 * The cached value of the '{@link #getDefaultInterchange() <em>Default Interchange</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDefaultInterchange()
	 * @generated
	 * @ordered
	 */
	protected DataInterchange defaultInterchange;

	/**
	 * The default value of the '{@link #isHasFileMask() <em>Has File Mask</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isHasFileMask()
	 * @generated
	 * @ordered
	 */
	protected static final boolean HAS_FILE_MASK_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isHasFileMask() <em>Has File Mask</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isHasFileMask()
	 * @generated
	 * @ordered
	 */
	protected boolean hasFileMask = HAS_FILE_MASK_EDEFAULT;

	/**
	 * The default value of the '{@link #getIdentifier() <em>Identifier</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIdentifier()
	 * @generated
	 * @ordered
	 */
	protected static final String IDENTIFIER_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getIdentifier() <em>Identifier</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIdentifier()
	 * @generated
	 * @ordered
	 */
	protected String identifier = IDENTIFIER_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SignalWatcherImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SignalDSLPackage.Literals.SIGNAL_WATCHER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataInterchange getDefaultInterchange() {
		if (defaultInterchange != null && defaultInterchange.eIsProxy()) {
			InternalEObject oldDefaultInterchange = (InternalEObject)defaultInterchange;
			defaultInterchange = (DataInterchange)eResolveProxy(oldDefaultInterchange);
			if (defaultInterchange != oldDefaultInterchange) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, SignalDSLPackage.SIGNAL_WATCHER__DEFAULT_INTERCHANGE, oldDefaultInterchange, defaultInterchange));
			}
		}
		return defaultInterchange;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataInterchange basicGetDefaultInterchange() {
		return defaultInterchange;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDefaultInterchange(DataInterchange newDefaultInterchange) {
		DataInterchange oldDefaultInterchange = defaultInterchange;
		defaultInterchange = newDefaultInterchange;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SignalDSLPackage.SIGNAL_WATCHER__DEFAULT_INTERCHANGE, oldDefaultInterchange, defaultInterchange));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isHasFileMask() {
		return hasFileMask;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHasFileMask(boolean newHasFileMask) {
		boolean oldHasFileMask = hasFileMask;
		hasFileMask = newHasFileMask;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SignalDSLPackage.SIGNAL_WATCHER__HAS_FILE_MASK, oldHasFileMask, hasFileMask));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getIdentifier() {
		return identifier;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIdentifier(String newIdentifier) {
		String oldIdentifier = identifier;
		identifier = newIdentifier;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SignalDSLPackage.SIGNAL_WATCHER__IDENTIFIER, oldIdentifier, identifier));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SignalDSLPackage.SIGNAL_WATCHER__DEFAULT_INTERCHANGE:
				if (resolve) return getDefaultInterchange();
				return basicGetDefaultInterchange();
			case SignalDSLPackage.SIGNAL_WATCHER__HAS_FILE_MASK:
				return isHasFileMask();
			case SignalDSLPackage.SIGNAL_WATCHER__IDENTIFIER:
				return getIdentifier();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SignalDSLPackage.SIGNAL_WATCHER__DEFAULT_INTERCHANGE:
				setDefaultInterchange((DataInterchange)newValue);
				return;
			case SignalDSLPackage.SIGNAL_WATCHER__HAS_FILE_MASK:
				setHasFileMask((Boolean)newValue);
				return;
			case SignalDSLPackage.SIGNAL_WATCHER__IDENTIFIER:
				setIdentifier((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SignalDSLPackage.SIGNAL_WATCHER__DEFAULT_INTERCHANGE:
				setDefaultInterchange((DataInterchange)null);
				return;
			case SignalDSLPackage.SIGNAL_WATCHER__HAS_FILE_MASK:
				setHasFileMask(HAS_FILE_MASK_EDEFAULT);
				return;
			case SignalDSLPackage.SIGNAL_WATCHER__IDENTIFIER:
				setIdentifier(IDENTIFIER_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SignalDSLPackage.SIGNAL_WATCHER__DEFAULT_INTERCHANGE:
				return defaultInterchange != null;
			case SignalDSLPackage.SIGNAL_WATCHER__HAS_FILE_MASK:
				return hasFileMask != HAS_FILE_MASK_EDEFAULT;
			case SignalDSLPackage.SIGNAL_WATCHER__IDENTIFIER:
				return IDENTIFIER_EDEFAULT == null ? identifier != null : !IDENTIFIER_EDEFAULT.equals(identifier);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (hasFileMask: ");
		result.append(hasFileMask);
		result.append(", identifier: ");
		result.append(identifier);
		result.append(')');
		return result.toString();
	}

} //SignalWatcherImpl
