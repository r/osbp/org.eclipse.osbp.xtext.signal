/**
 * Copyright (c) 2011, 2018 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0 
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:                                                      
 *     Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 *  
 *  generated from SignalDSL.xcore
 * 
 *  
 */
package org.eclipse.osbp.xtext.signal.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.osbp.xtext.signal.SchedulerType;
import org.eclipse.osbp.xtext.signal.SignalDSLPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Scheduler Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class SchedulerTypeImpl extends SignalLazyResolverImpl implements SchedulerType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SchedulerTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SignalDSLPackage.Literals.SCHEDULER_TYPE;
	}

} //SchedulerTypeImpl
