/**
 * Copyright (c) 2011, 2018 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0 
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:                                                      
 *     Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 *  
 *  generated from SignalDSL.xcore
 * 
 *  
 */
package org.eclipse.osbp.xtext.signal.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.osbp.xtext.signal.SignalDSLPackage;
import org.eclipse.osbp.xtext.signal.SignalTask;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Signal Task</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class SignalTaskImpl extends SignalLazyResolverImpl implements SignalTask {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SignalTaskImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SignalDSLPackage.Literals.SIGNAL_TASK;
	}

} //SignalTaskImpl
