/**
 * Copyright (c) 2011, 2018 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0 
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:                                                      
 *     Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 *  
 *  generated from SignalDSL.xcore
 * 
 *  
 */
package org.eclipse.osbp.xtext.signal.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.osbp.xtext.signal.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class SignalDSLFactoryImpl extends EFactoryImpl implements SignalDSLFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static SignalDSLFactory init() {
		try {
			SignalDSLFactory theSignalDSLFactory = (SignalDSLFactory)EPackage.Registry.INSTANCE.getEFactory(SignalDSLPackage.eNS_URI);
			if (theSignalDSLFactory != null) {
				return theSignalDSLFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new SignalDSLFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SignalDSLFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case SignalDSLPackage.SIGNAL_MODEL: return createSignalModel();
			case SignalDSLPackage.SIGNAL_LAZY_RESOLVER: return createSignalLazyResolver();
			case SignalDSLPackage.SIGNAL_PACKAGE: return createSignalPackage();
			case SignalDSLPackage.SIGNAL_DEFINITION: return createSignalDefinition();
			case SignalDSLPackage.SIGNAL_WATCHER: return createSignalWatcher();
			case SignalDSLPackage.SIGNAL_SCHEDULER: return createSignalScheduler();
			case SignalDSLPackage.SCHEDULER_TYPE: return createSchedulerType();
			case SignalDSLPackage.CRON_SCHEDULER: return createCronScheduler();
			case SignalDSLPackage.HOURLY_SCHEDULER: return createHourlyScheduler();
			case SignalDSLPackage.DAILY_SCHEDULER: return createDailyScheduler();
			case SignalDSLPackage.WEEKLY_SCHEDULER: return createWeeklyScheduler();
			case SignalDSLPackage.MONTHLY_SCHEDULER: return createMonthlyScheduler();
			case SignalDSLPackage.SIGNAL_TASK: return createSignalTask();
			case SignalDSLPackage.SIGNAL_DATAINTERCHANGE: return createSignalDatainterchange();
			case SignalDSLPackage.SIGNAL_FUNCTION: return createSignalFunction();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case SignalDSLPackage.SIGNAL_ACTION_TYPE_ENUM:
				return createSignalActionTypeEnumFromString(eDataType, initialValue);
			case SignalDSLPackage.SIGNAL_EXECUTION_TYPE_ENUM:
				return createSignalExecutionTypeEnumFromString(eDataType, initialValue);
			case SignalDSLPackage.DAY_OF_WEEK_ENUM:
				return createDayOfWeekEnumFromString(eDataType, initialValue);
			case SignalDSLPackage.INTERNAL_EOBJECT:
				return createInternalEObjectFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case SignalDSLPackage.SIGNAL_ACTION_TYPE_ENUM:
				return convertSignalActionTypeEnumToString(eDataType, instanceValue);
			case SignalDSLPackage.SIGNAL_EXECUTION_TYPE_ENUM:
				return convertSignalExecutionTypeEnumToString(eDataType, instanceValue);
			case SignalDSLPackage.DAY_OF_WEEK_ENUM:
				return convertDayOfWeekEnumToString(eDataType, instanceValue);
			case SignalDSLPackage.INTERNAL_EOBJECT:
				return convertInternalEObjectToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SignalModel createSignalModel() {
		SignalModelImpl signalModel = new SignalModelImpl();
		return signalModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SignalLazyResolver createSignalLazyResolver() {
		SignalLazyResolverImpl signalLazyResolver = new SignalLazyResolverImpl();
		return signalLazyResolver;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SignalPackage createSignalPackage() {
		SignalPackageImpl signalPackage = new SignalPackageImpl();
		return signalPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SignalDefinition createSignalDefinition() {
		SignalDefinitionImpl signalDefinition = new SignalDefinitionImpl();
		return signalDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SignalWatcher createSignalWatcher() {
		SignalWatcherImpl signalWatcher = new SignalWatcherImpl();
		return signalWatcher;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SignalScheduler createSignalScheduler() {
		SignalSchedulerImpl signalScheduler = new SignalSchedulerImpl();
		return signalScheduler;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SchedulerType createSchedulerType() {
		SchedulerTypeImpl schedulerType = new SchedulerTypeImpl();
		return schedulerType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CronScheduler createCronScheduler() {
		CronSchedulerImpl cronScheduler = new CronSchedulerImpl();
		return cronScheduler;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HourlyScheduler createHourlyScheduler() {
		HourlySchedulerImpl hourlyScheduler = new HourlySchedulerImpl();
		return hourlyScheduler;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DailyScheduler createDailyScheduler() {
		DailySchedulerImpl dailyScheduler = new DailySchedulerImpl();
		return dailyScheduler;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WeeklyScheduler createWeeklyScheduler() {
		WeeklySchedulerImpl weeklyScheduler = new WeeklySchedulerImpl();
		return weeklyScheduler;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MonthlyScheduler createMonthlyScheduler() {
		MonthlySchedulerImpl monthlyScheduler = new MonthlySchedulerImpl();
		return monthlyScheduler;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SignalTask createSignalTask() {
		SignalTaskImpl signalTask = new SignalTaskImpl();
		return signalTask;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SignalDatainterchange createSignalDatainterchange() {
		SignalDatainterchangeImpl signalDatainterchange = new SignalDatainterchangeImpl();
		return signalDatainterchange;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SignalFunction createSignalFunction() {
		SignalFunctionImpl signalFunction = new SignalFunctionImpl();
		return signalFunction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SignalActionTypeEnum createSignalActionTypeEnumFromString(EDataType eDataType, String initialValue) {
		SignalActionTypeEnum result = SignalActionTypeEnum.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertSignalActionTypeEnumToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SignalExecutionTypeEnum createSignalExecutionTypeEnumFromString(EDataType eDataType, String initialValue) {
		SignalExecutionTypeEnum result = SignalExecutionTypeEnum.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertSignalExecutionTypeEnumToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DayOfWeekEnum createDayOfWeekEnumFromString(EDataType eDataType, String initialValue) {
		DayOfWeekEnum result = DayOfWeekEnum.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertDayOfWeekEnumToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InternalEObject createInternalEObjectFromString(EDataType eDataType, String initialValue) {
		return (InternalEObject)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertInternalEObjectToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SignalDSLPackage getSignalDSLPackage() {
		return (SignalDSLPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static SignalDSLPackage getPackage() {
		return SignalDSLPackage.eINSTANCE;
	}

} //SignalDSLFactoryImpl
