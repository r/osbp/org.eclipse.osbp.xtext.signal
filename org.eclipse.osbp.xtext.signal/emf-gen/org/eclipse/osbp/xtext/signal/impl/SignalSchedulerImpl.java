/**
 * Copyright (c) 2011, 2018 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0 
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:                                                      
 *     Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 *  
 *  generated from SignalDSL.xcore
 * 
 *  
 */
package org.eclipse.osbp.xtext.signal.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.osbp.xtext.signal.SchedulerType;
import org.eclipse.osbp.xtext.signal.SignalDSLPackage;
import org.eclipse.osbp.xtext.signal.SignalScheduler;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Signal Scheduler</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.signal.impl.SignalSchedulerImpl#getSchedulertype <em>Schedulertype</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SignalSchedulerImpl extends SignalDefinitionImpl implements SignalScheduler {
	/**
	 * The cached value of the '{@link #getSchedulertype() <em>Schedulertype</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSchedulertype()
	 * @generated
	 * @ordered
	 */
	protected SchedulerType schedulertype;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SignalSchedulerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SignalDSLPackage.Literals.SIGNAL_SCHEDULER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SchedulerType getSchedulertype() {
		return schedulertype;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSchedulertype(SchedulerType newSchedulertype, NotificationChain msgs) {
		SchedulerType oldSchedulertype = schedulertype;
		schedulertype = newSchedulertype;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SignalDSLPackage.SIGNAL_SCHEDULER__SCHEDULERTYPE, oldSchedulertype, newSchedulertype);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSchedulertype(SchedulerType newSchedulertype) {
		if (newSchedulertype != schedulertype) {
			NotificationChain msgs = null;
			if (schedulertype != null)
				msgs = ((InternalEObject)schedulertype).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SignalDSLPackage.SIGNAL_SCHEDULER__SCHEDULERTYPE, null, msgs);
			if (newSchedulertype != null)
				msgs = ((InternalEObject)newSchedulertype).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SignalDSLPackage.SIGNAL_SCHEDULER__SCHEDULERTYPE, null, msgs);
			msgs = basicSetSchedulertype(newSchedulertype, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SignalDSLPackage.SIGNAL_SCHEDULER__SCHEDULERTYPE, newSchedulertype, newSchedulertype));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case SignalDSLPackage.SIGNAL_SCHEDULER__SCHEDULERTYPE:
				return basicSetSchedulertype(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SignalDSLPackage.SIGNAL_SCHEDULER__SCHEDULERTYPE:
				return getSchedulertype();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SignalDSLPackage.SIGNAL_SCHEDULER__SCHEDULERTYPE:
				setSchedulertype((SchedulerType)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SignalDSLPackage.SIGNAL_SCHEDULER__SCHEDULERTYPE:
				setSchedulertype((SchedulerType)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SignalDSLPackage.SIGNAL_SCHEDULER__SCHEDULERTYPE:
				return schedulertype != null;
		}
		return super.eIsSet(featureID);
	}

} //SignalSchedulerImpl
