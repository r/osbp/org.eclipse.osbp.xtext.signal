/**
 * Copyright (c) 2011, 2018 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0 
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:                                                      
 *     Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 *  
 *  generated from SignalDSL.xcore
 * 
 *  
 */
package org.eclipse.osbp.xtext.signal.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.osbp.xtext.datainterchange.DataInterchange;

import org.eclipse.osbp.xtext.functionlibrarydsl.FunctionLibraryActionGroup;
import org.eclipse.osbp.xtext.functionlibrarydsl.FunctionLibraryExecute;

import org.eclipse.osbp.xtext.signal.SignalDSLPackage;
import org.eclipse.osbp.xtext.signal.SignalFunction;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Signal Function</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.signal.impl.SignalFunctionImpl#getGroup <em>Group</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.signal.impl.SignalFunctionImpl#getDoExecuteFunction <em>Do Execute Function</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.signal.impl.SignalFunctionImpl#isOnImportFile <em>On Import File</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.signal.impl.SignalFunctionImpl#isOnExportFile <em>On Export File</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.signal.impl.SignalFunctionImpl#getSupportInterchange <em>Support Interchange</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SignalFunctionImpl extends SignalTaskImpl implements SignalFunction {
	/**
	 * The cached value of the '{@link #getGroup() <em>Group</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGroup()
	 * @generated
	 * @ordered
	 */
	protected FunctionLibraryActionGroup group;

	/**
	 * The cached value of the '{@link #getDoExecuteFunction() <em>Do Execute Function</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDoExecuteFunction()
	 * @generated
	 * @ordered
	 */
	protected FunctionLibraryExecute doExecuteFunction;

	/**
	 * The default value of the '{@link #isOnImportFile() <em>On Import File</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isOnImportFile()
	 * @generated
	 * @ordered
	 */
	protected static final boolean ON_IMPORT_FILE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isOnImportFile() <em>On Import File</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isOnImportFile()
	 * @generated
	 * @ordered
	 */
	protected boolean onImportFile = ON_IMPORT_FILE_EDEFAULT;

	/**
	 * The default value of the '{@link #isOnExportFile() <em>On Export File</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isOnExportFile()
	 * @generated
	 * @ordered
	 */
	protected static final boolean ON_EXPORT_FILE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isOnExportFile() <em>On Export File</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isOnExportFile()
	 * @generated
	 * @ordered
	 */
	protected boolean onExportFile = ON_EXPORT_FILE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getSupportInterchange() <em>Support Interchange</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSupportInterchange()
	 * @generated
	 * @ordered
	 */
	protected DataInterchange supportInterchange;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SignalFunctionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SignalDSLPackage.Literals.SIGNAL_FUNCTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionLibraryActionGroup getGroup() {
		if (group != null && group.eIsProxy()) {
			InternalEObject oldGroup = (InternalEObject)group;
			group = (FunctionLibraryActionGroup)eResolveProxy(oldGroup);
			if (group != oldGroup) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, SignalDSLPackage.SIGNAL_FUNCTION__GROUP, oldGroup, group));
			}
		}
		return group;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionLibraryActionGroup basicGetGroup() {
		return group;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGroup(FunctionLibraryActionGroup newGroup) {
		FunctionLibraryActionGroup oldGroup = group;
		group = newGroup;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SignalDSLPackage.SIGNAL_FUNCTION__GROUP, oldGroup, group));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionLibraryExecute getDoExecuteFunction() {
		if (doExecuteFunction != null && doExecuteFunction.eIsProxy()) {
			InternalEObject oldDoExecuteFunction = (InternalEObject)doExecuteFunction;
			doExecuteFunction = (FunctionLibraryExecute)eResolveProxy(oldDoExecuteFunction);
			if (doExecuteFunction != oldDoExecuteFunction) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, SignalDSLPackage.SIGNAL_FUNCTION__DO_EXECUTE_FUNCTION, oldDoExecuteFunction, doExecuteFunction));
			}
		}
		return doExecuteFunction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionLibraryExecute basicGetDoExecuteFunction() {
		return doExecuteFunction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDoExecuteFunction(FunctionLibraryExecute newDoExecuteFunction) {
		FunctionLibraryExecute oldDoExecuteFunction = doExecuteFunction;
		doExecuteFunction = newDoExecuteFunction;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SignalDSLPackage.SIGNAL_FUNCTION__DO_EXECUTE_FUNCTION, oldDoExecuteFunction, doExecuteFunction));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isOnImportFile() {
		return onImportFile;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOnImportFile(boolean newOnImportFile) {
		boolean oldOnImportFile = onImportFile;
		onImportFile = newOnImportFile;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SignalDSLPackage.SIGNAL_FUNCTION__ON_IMPORT_FILE, oldOnImportFile, onImportFile));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isOnExportFile() {
		return onExportFile;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOnExportFile(boolean newOnExportFile) {
		boolean oldOnExportFile = onExportFile;
		onExportFile = newOnExportFile;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SignalDSLPackage.SIGNAL_FUNCTION__ON_EXPORT_FILE, oldOnExportFile, onExportFile));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataInterchange getSupportInterchange() {
		if (supportInterchange != null && supportInterchange.eIsProxy()) {
			InternalEObject oldSupportInterchange = (InternalEObject)supportInterchange;
			supportInterchange = (DataInterchange)eResolveProxy(oldSupportInterchange);
			if (supportInterchange != oldSupportInterchange) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, SignalDSLPackage.SIGNAL_FUNCTION__SUPPORT_INTERCHANGE, oldSupportInterchange, supportInterchange));
			}
		}
		return supportInterchange;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataInterchange basicGetSupportInterchange() {
		return supportInterchange;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSupportInterchange(DataInterchange newSupportInterchange) {
		DataInterchange oldSupportInterchange = supportInterchange;
		supportInterchange = newSupportInterchange;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SignalDSLPackage.SIGNAL_FUNCTION__SUPPORT_INTERCHANGE, oldSupportInterchange, supportInterchange));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SignalDSLPackage.SIGNAL_FUNCTION__GROUP:
				if (resolve) return getGroup();
				return basicGetGroup();
			case SignalDSLPackage.SIGNAL_FUNCTION__DO_EXECUTE_FUNCTION:
				if (resolve) return getDoExecuteFunction();
				return basicGetDoExecuteFunction();
			case SignalDSLPackage.SIGNAL_FUNCTION__ON_IMPORT_FILE:
				return isOnImportFile();
			case SignalDSLPackage.SIGNAL_FUNCTION__ON_EXPORT_FILE:
				return isOnExportFile();
			case SignalDSLPackage.SIGNAL_FUNCTION__SUPPORT_INTERCHANGE:
				if (resolve) return getSupportInterchange();
				return basicGetSupportInterchange();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SignalDSLPackage.SIGNAL_FUNCTION__GROUP:
				setGroup((FunctionLibraryActionGroup)newValue);
				return;
			case SignalDSLPackage.SIGNAL_FUNCTION__DO_EXECUTE_FUNCTION:
				setDoExecuteFunction((FunctionLibraryExecute)newValue);
				return;
			case SignalDSLPackage.SIGNAL_FUNCTION__ON_IMPORT_FILE:
				setOnImportFile((Boolean)newValue);
				return;
			case SignalDSLPackage.SIGNAL_FUNCTION__ON_EXPORT_FILE:
				setOnExportFile((Boolean)newValue);
				return;
			case SignalDSLPackage.SIGNAL_FUNCTION__SUPPORT_INTERCHANGE:
				setSupportInterchange((DataInterchange)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SignalDSLPackage.SIGNAL_FUNCTION__GROUP:
				setGroup((FunctionLibraryActionGroup)null);
				return;
			case SignalDSLPackage.SIGNAL_FUNCTION__DO_EXECUTE_FUNCTION:
				setDoExecuteFunction((FunctionLibraryExecute)null);
				return;
			case SignalDSLPackage.SIGNAL_FUNCTION__ON_IMPORT_FILE:
				setOnImportFile(ON_IMPORT_FILE_EDEFAULT);
				return;
			case SignalDSLPackage.SIGNAL_FUNCTION__ON_EXPORT_FILE:
				setOnExportFile(ON_EXPORT_FILE_EDEFAULT);
				return;
			case SignalDSLPackage.SIGNAL_FUNCTION__SUPPORT_INTERCHANGE:
				setSupportInterchange((DataInterchange)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SignalDSLPackage.SIGNAL_FUNCTION__GROUP:
				return group != null;
			case SignalDSLPackage.SIGNAL_FUNCTION__DO_EXECUTE_FUNCTION:
				return doExecuteFunction != null;
			case SignalDSLPackage.SIGNAL_FUNCTION__ON_IMPORT_FILE:
				return onImportFile != ON_IMPORT_FILE_EDEFAULT;
			case SignalDSLPackage.SIGNAL_FUNCTION__ON_EXPORT_FILE:
				return onExportFile != ON_EXPORT_FILE_EDEFAULT;
			case SignalDSLPackage.SIGNAL_FUNCTION__SUPPORT_INTERCHANGE:
				return supportInterchange != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (onImportFile: ");
		result.append(onImportFile);
		result.append(", onExportFile: ");
		result.append(onExportFile);
		result.append(')');
		return result.toString();
	}

} //SignalFunctionImpl
