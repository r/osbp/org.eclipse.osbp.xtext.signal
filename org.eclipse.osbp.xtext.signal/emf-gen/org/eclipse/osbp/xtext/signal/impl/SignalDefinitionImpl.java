/**
 * Copyright (c) 2011, 2018 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0 
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:                                                      
 *     Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 *  
 *  generated from SignalDSL.xcore
 * 
 *  
 */
package org.eclipse.osbp.xtext.signal.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.osbp.xtext.datainterchange.DataInterchangeGroup;

import org.eclipse.osbp.xtext.signal.SignalDSLPackage;
import org.eclipse.osbp.xtext.signal.SignalDefinition;
import org.eclipse.osbp.xtext.signal.SignalExecutionTypeEnum;
import org.eclipse.osbp.xtext.signal.SignalTask;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Signal Definition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.signal.impl.SignalDefinitionImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.signal.impl.SignalDefinitionImpl#getInterchangegroup <em>Interchangegroup</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.signal.impl.SignalDefinitionImpl#getExecutiontype <em>Executiontype</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.signal.impl.SignalDefinitionImpl#getTasks <em>Tasks</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SignalDefinitionImpl extends SignalLazyResolverImpl implements SignalDefinition {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getInterchangegroup() <em>Interchangegroup</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInterchangegroup()
	 * @generated
	 * @ordered
	 */
	protected DataInterchangeGroup interchangegroup;

	/**
	 * The default value of the '{@link #getExecutiontype() <em>Executiontype</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExecutiontype()
	 * @generated
	 * @ordered
	 */
	protected static final SignalExecutionTypeEnum EXECUTIONTYPE_EDEFAULT = SignalExecutionTypeEnum.SEQ;

	/**
	 * The cached value of the '{@link #getExecutiontype() <em>Executiontype</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExecutiontype()
	 * @generated
	 * @ordered
	 */
	protected SignalExecutionTypeEnum executiontype = EXECUTIONTYPE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getTasks() <em>Tasks</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTasks()
	 * @generated
	 * @ordered
	 */
	protected EList<SignalTask> tasks;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SignalDefinitionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SignalDSLPackage.Literals.SIGNAL_DEFINITION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SignalDSLPackage.SIGNAL_DEFINITION__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataInterchangeGroup getInterchangegroup() {
		if (interchangegroup != null && interchangegroup.eIsProxy()) {
			InternalEObject oldInterchangegroup = (InternalEObject)interchangegroup;
			interchangegroup = (DataInterchangeGroup)eResolveProxy(oldInterchangegroup);
			if (interchangegroup != oldInterchangegroup) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, SignalDSLPackage.SIGNAL_DEFINITION__INTERCHANGEGROUP, oldInterchangegroup, interchangegroup));
			}
		}
		return interchangegroup;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataInterchangeGroup basicGetInterchangegroup() {
		return interchangegroup;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInterchangegroup(DataInterchangeGroup newInterchangegroup) {
		DataInterchangeGroup oldInterchangegroup = interchangegroup;
		interchangegroup = newInterchangegroup;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SignalDSLPackage.SIGNAL_DEFINITION__INTERCHANGEGROUP, oldInterchangegroup, interchangegroup));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SignalExecutionTypeEnum getExecutiontype() {
		return executiontype;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExecutiontype(SignalExecutionTypeEnum newExecutiontype) {
		SignalExecutionTypeEnum oldExecutiontype = executiontype;
		executiontype = newExecutiontype == null ? EXECUTIONTYPE_EDEFAULT : newExecutiontype;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SignalDSLPackage.SIGNAL_DEFINITION__EXECUTIONTYPE, oldExecutiontype, executiontype));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SignalTask> getTasks() {
		if (tasks == null) {
			tasks = new EObjectContainmentEList<SignalTask>(SignalTask.class, this, SignalDSLPackage.SIGNAL_DEFINITION__TASKS);
		}
		return tasks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case SignalDSLPackage.SIGNAL_DEFINITION__TASKS:
				return ((InternalEList<?>)getTasks()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SignalDSLPackage.SIGNAL_DEFINITION__NAME:
				return getName();
			case SignalDSLPackage.SIGNAL_DEFINITION__INTERCHANGEGROUP:
				if (resolve) return getInterchangegroup();
				return basicGetInterchangegroup();
			case SignalDSLPackage.SIGNAL_DEFINITION__EXECUTIONTYPE:
				return getExecutiontype();
			case SignalDSLPackage.SIGNAL_DEFINITION__TASKS:
				return getTasks();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SignalDSLPackage.SIGNAL_DEFINITION__NAME:
				setName((String)newValue);
				return;
			case SignalDSLPackage.SIGNAL_DEFINITION__INTERCHANGEGROUP:
				setInterchangegroup((DataInterchangeGroup)newValue);
				return;
			case SignalDSLPackage.SIGNAL_DEFINITION__EXECUTIONTYPE:
				setExecutiontype((SignalExecutionTypeEnum)newValue);
				return;
			case SignalDSLPackage.SIGNAL_DEFINITION__TASKS:
				getTasks().clear();
				getTasks().addAll((Collection<? extends SignalTask>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SignalDSLPackage.SIGNAL_DEFINITION__NAME:
				setName(NAME_EDEFAULT);
				return;
			case SignalDSLPackage.SIGNAL_DEFINITION__INTERCHANGEGROUP:
				setInterchangegroup((DataInterchangeGroup)null);
				return;
			case SignalDSLPackage.SIGNAL_DEFINITION__EXECUTIONTYPE:
				setExecutiontype(EXECUTIONTYPE_EDEFAULT);
				return;
			case SignalDSLPackage.SIGNAL_DEFINITION__TASKS:
				getTasks().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SignalDSLPackage.SIGNAL_DEFINITION__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case SignalDSLPackage.SIGNAL_DEFINITION__INTERCHANGEGROUP:
				return interchangegroup != null;
			case SignalDSLPackage.SIGNAL_DEFINITION__EXECUTIONTYPE:
				return executiontype != EXECUTIONTYPE_EDEFAULT;
			case SignalDSLPackage.SIGNAL_DEFINITION__TASKS:
				return tasks != null && !tasks.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", executiontype: ");
		result.append(executiontype);
		result.append(')');
		return result.toString();
	}

} //SignalDefinitionImpl
