/**
 * Copyright (c) 2011, 2018 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0 
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:                                                      
 *     Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 *  
 *  generated from SignalDSL.xcore
 * 
 *  
 */
package org.eclipse.osbp.xtext.signal.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.osbp.xtext.datainterchange.DataInterchange;

import org.eclipse.osbp.xtext.signal.SignalActionTypeEnum;
import org.eclipse.osbp.xtext.signal.SignalDSLPackage;
import org.eclipse.osbp.xtext.signal.SignalDatainterchange;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Signal Datainterchange</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.signal.impl.SignalDatainterchangeImpl#getActionType <em>Action Type</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.signal.impl.SignalDatainterchangeImpl#getDataRef <em>Data Ref</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.signal.impl.SignalDatainterchangeImpl#isBaseinterchange <em>Baseinterchange</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SignalDatainterchangeImpl extends SignalTaskImpl implements SignalDatainterchange {
	/**
	 * The default value of the '{@link #getActionType() <em>Action Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActionType()
	 * @generated
	 * @ordered
	 */
	protected static final SignalActionTypeEnum ACTION_TYPE_EDEFAULT = SignalActionTypeEnum.DATAIMPORT;

	/**
	 * The cached value of the '{@link #getActionType() <em>Action Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActionType()
	 * @generated
	 * @ordered
	 */
	protected SignalActionTypeEnum actionType = ACTION_TYPE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getDataRef() <em>Data Ref</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDataRef()
	 * @generated
	 * @ordered
	 */
	protected DataInterchange dataRef;

	/**
	 * The default value of the '{@link #isBaseinterchange() <em>Baseinterchange</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isBaseinterchange()
	 * @generated
	 * @ordered
	 */
	protected static final boolean BASEINTERCHANGE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isBaseinterchange() <em>Baseinterchange</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isBaseinterchange()
	 * @generated
	 * @ordered
	 */
	protected boolean baseinterchange = BASEINTERCHANGE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SignalDatainterchangeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SignalDSLPackage.Literals.SIGNAL_DATAINTERCHANGE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SignalActionTypeEnum getActionType() {
		return actionType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setActionType(SignalActionTypeEnum newActionType) {
		SignalActionTypeEnum oldActionType = actionType;
		actionType = newActionType == null ? ACTION_TYPE_EDEFAULT : newActionType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SignalDSLPackage.SIGNAL_DATAINTERCHANGE__ACTION_TYPE, oldActionType, actionType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataInterchange getDataRef() {
		if (dataRef != null && dataRef.eIsProxy()) {
			InternalEObject oldDataRef = (InternalEObject)dataRef;
			dataRef = (DataInterchange)eResolveProxy(oldDataRef);
			if (dataRef != oldDataRef) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, SignalDSLPackage.SIGNAL_DATAINTERCHANGE__DATA_REF, oldDataRef, dataRef));
			}
		}
		return dataRef;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataInterchange basicGetDataRef() {
		return dataRef;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDataRef(DataInterchange newDataRef) {
		DataInterchange oldDataRef = dataRef;
		dataRef = newDataRef;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SignalDSLPackage.SIGNAL_DATAINTERCHANGE__DATA_REF, oldDataRef, dataRef));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isBaseinterchange() {
		return baseinterchange;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBaseinterchange(boolean newBaseinterchange) {
		boolean oldBaseinterchange = baseinterchange;
		baseinterchange = newBaseinterchange;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SignalDSLPackage.SIGNAL_DATAINTERCHANGE__BASEINTERCHANGE, oldBaseinterchange, baseinterchange));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SignalDSLPackage.SIGNAL_DATAINTERCHANGE__ACTION_TYPE:
				return getActionType();
			case SignalDSLPackage.SIGNAL_DATAINTERCHANGE__DATA_REF:
				if (resolve) return getDataRef();
				return basicGetDataRef();
			case SignalDSLPackage.SIGNAL_DATAINTERCHANGE__BASEINTERCHANGE:
				return isBaseinterchange();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SignalDSLPackage.SIGNAL_DATAINTERCHANGE__ACTION_TYPE:
				setActionType((SignalActionTypeEnum)newValue);
				return;
			case SignalDSLPackage.SIGNAL_DATAINTERCHANGE__DATA_REF:
				setDataRef((DataInterchange)newValue);
				return;
			case SignalDSLPackage.SIGNAL_DATAINTERCHANGE__BASEINTERCHANGE:
				setBaseinterchange((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SignalDSLPackage.SIGNAL_DATAINTERCHANGE__ACTION_TYPE:
				setActionType(ACTION_TYPE_EDEFAULT);
				return;
			case SignalDSLPackage.SIGNAL_DATAINTERCHANGE__DATA_REF:
				setDataRef((DataInterchange)null);
				return;
			case SignalDSLPackage.SIGNAL_DATAINTERCHANGE__BASEINTERCHANGE:
				setBaseinterchange(BASEINTERCHANGE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SignalDSLPackage.SIGNAL_DATAINTERCHANGE__ACTION_TYPE:
				return actionType != ACTION_TYPE_EDEFAULT;
			case SignalDSLPackage.SIGNAL_DATAINTERCHANGE__DATA_REF:
				return dataRef != null;
			case SignalDSLPackage.SIGNAL_DATAINTERCHANGE__BASEINTERCHANGE:
				return baseinterchange != BASEINTERCHANGE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (actionType: ");
		result.append(actionType);
		result.append(", baseinterchange: ");
		result.append(baseinterchange);
		result.append(')');
		return result.toString();
	}

} //SignalDatainterchangeImpl
