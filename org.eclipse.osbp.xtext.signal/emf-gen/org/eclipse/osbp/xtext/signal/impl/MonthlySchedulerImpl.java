/**
 * Copyright (c) 2011, 2018 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0 
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:                                                      
 *     Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 *  
 *  generated from SignalDSL.xcore
 * 
 *  
 */
package org.eclipse.osbp.xtext.signal.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.osbp.xtext.signal.MonthlyScheduler;
import org.eclipse.osbp.xtext.signal.SignalDSLPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Monthly Scheduler</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.signal.impl.MonthlySchedulerImpl#getDayofmonth <em>Dayofmonth</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.signal.impl.MonthlySchedulerImpl#getHour <em>Hour</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.signal.impl.MonthlySchedulerImpl#getMinute <em>Minute</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MonthlySchedulerImpl extends SchedulerTypeImpl implements MonthlyScheduler {
	/**
	 * The default value of the '{@link #getDayofmonth() <em>Dayofmonth</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDayofmonth()
	 * @generated
	 * @ordered
	 */
	protected static final int DAYOFMONTH_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getDayofmonth() <em>Dayofmonth</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDayofmonth()
	 * @generated
	 * @ordered
	 */
	protected int dayofmonth = DAYOFMONTH_EDEFAULT;

	/**
	 * The default value of the '{@link #getHour() <em>Hour</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHour()
	 * @generated
	 * @ordered
	 */
	protected static final int HOUR_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getHour() <em>Hour</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHour()
	 * @generated
	 * @ordered
	 */
	protected int hour = HOUR_EDEFAULT;

	/**
	 * The default value of the '{@link #getMinute() <em>Minute</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMinute()
	 * @generated
	 * @ordered
	 */
	protected static final int MINUTE_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getMinute() <em>Minute</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMinute()
	 * @generated
	 * @ordered
	 */
	protected int minute = MINUTE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MonthlySchedulerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SignalDSLPackage.Literals.MONTHLY_SCHEDULER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getDayofmonth() {
		return dayofmonth;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDayofmonth(int newDayofmonth) {
		int oldDayofmonth = dayofmonth;
		dayofmonth = newDayofmonth;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SignalDSLPackage.MONTHLY_SCHEDULER__DAYOFMONTH, oldDayofmonth, dayofmonth));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getHour() {
		return hour;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHour(int newHour) {
		int oldHour = hour;
		hour = newHour;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SignalDSLPackage.MONTHLY_SCHEDULER__HOUR, oldHour, hour));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getMinute() {
		return minute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMinute(int newMinute) {
		int oldMinute = minute;
		minute = newMinute;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SignalDSLPackage.MONTHLY_SCHEDULER__MINUTE, oldMinute, minute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SignalDSLPackage.MONTHLY_SCHEDULER__DAYOFMONTH:
				return getDayofmonth();
			case SignalDSLPackage.MONTHLY_SCHEDULER__HOUR:
				return getHour();
			case SignalDSLPackage.MONTHLY_SCHEDULER__MINUTE:
				return getMinute();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SignalDSLPackage.MONTHLY_SCHEDULER__DAYOFMONTH:
				setDayofmonth((Integer)newValue);
				return;
			case SignalDSLPackage.MONTHLY_SCHEDULER__HOUR:
				setHour((Integer)newValue);
				return;
			case SignalDSLPackage.MONTHLY_SCHEDULER__MINUTE:
				setMinute((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SignalDSLPackage.MONTHLY_SCHEDULER__DAYOFMONTH:
				setDayofmonth(DAYOFMONTH_EDEFAULT);
				return;
			case SignalDSLPackage.MONTHLY_SCHEDULER__HOUR:
				setHour(HOUR_EDEFAULT);
				return;
			case SignalDSLPackage.MONTHLY_SCHEDULER__MINUTE:
				setMinute(MINUTE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SignalDSLPackage.MONTHLY_SCHEDULER__DAYOFMONTH:
				return dayofmonth != DAYOFMONTH_EDEFAULT;
			case SignalDSLPackage.MONTHLY_SCHEDULER__HOUR:
				return hour != HOUR_EDEFAULT;
			case SignalDSLPackage.MONTHLY_SCHEDULER__MINUTE:
				return minute != MINUTE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (dayofmonth: ");
		result.append(dayofmonth);
		result.append(", hour: ");
		result.append(hour);
		result.append(", minute: ");
		result.append(minute);
		result.append(')');
		return result.toString();
	}

} //MonthlySchedulerImpl
