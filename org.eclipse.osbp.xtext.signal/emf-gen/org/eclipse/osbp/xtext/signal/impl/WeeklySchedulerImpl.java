/**
 * Copyright (c) 2011, 2018 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0 
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:                                                      
 *     Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 *  
 *  generated from SignalDSL.xcore
 * 
 *  
 */
package org.eclipse.osbp.xtext.signal.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.osbp.xtext.signal.DayOfWeekEnum;
import org.eclipse.osbp.xtext.signal.SignalDSLPackage;
import org.eclipse.osbp.xtext.signal.WeeklyScheduler;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Weekly Scheduler</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.signal.impl.WeeklySchedulerImpl#getDayofweek <em>Dayofweek</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.signal.impl.WeeklySchedulerImpl#getHour <em>Hour</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.signal.impl.WeeklySchedulerImpl#getMinute <em>Minute</em>}</li>
 * </ul>
 *
 * @generated
 */
public class WeeklySchedulerImpl extends SchedulerTypeImpl implements WeeklyScheduler {
	/**
	 * The default value of the '{@link #getDayofweek() <em>Dayofweek</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDayofweek()
	 * @generated
	 * @ordered
	 */
	protected static final DayOfWeekEnum DAYOFWEEK_EDEFAULT = DayOfWeekEnum.SUNDAY;

	/**
	 * The cached value of the '{@link #getDayofweek() <em>Dayofweek</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDayofweek()
	 * @generated
	 * @ordered
	 */
	protected DayOfWeekEnum dayofweek = DAYOFWEEK_EDEFAULT;

	/**
	 * The default value of the '{@link #getHour() <em>Hour</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHour()
	 * @generated
	 * @ordered
	 */
	protected static final int HOUR_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getHour() <em>Hour</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHour()
	 * @generated
	 * @ordered
	 */
	protected int hour = HOUR_EDEFAULT;

	/**
	 * The default value of the '{@link #getMinute() <em>Minute</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMinute()
	 * @generated
	 * @ordered
	 */
	protected static final int MINUTE_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getMinute() <em>Minute</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMinute()
	 * @generated
	 * @ordered
	 */
	protected int minute = MINUTE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected WeeklySchedulerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SignalDSLPackage.Literals.WEEKLY_SCHEDULER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DayOfWeekEnum getDayofweek() {
		return dayofweek;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDayofweek(DayOfWeekEnum newDayofweek) {
		DayOfWeekEnum oldDayofweek = dayofweek;
		dayofweek = newDayofweek == null ? DAYOFWEEK_EDEFAULT : newDayofweek;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SignalDSLPackage.WEEKLY_SCHEDULER__DAYOFWEEK, oldDayofweek, dayofweek));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getHour() {
		return hour;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHour(int newHour) {
		int oldHour = hour;
		hour = newHour;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SignalDSLPackage.WEEKLY_SCHEDULER__HOUR, oldHour, hour));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getMinute() {
		return minute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMinute(int newMinute) {
		int oldMinute = minute;
		minute = newMinute;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SignalDSLPackage.WEEKLY_SCHEDULER__MINUTE, oldMinute, minute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SignalDSLPackage.WEEKLY_SCHEDULER__DAYOFWEEK:
				return getDayofweek();
			case SignalDSLPackage.WEEKLY_SCHEDULER__HOUR:
				return getHour();
			case SignalDSLPackage.WEEKLY_SCHEDULER__MINUTE:
				return getMinute();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SignalDSLPackage.WEEKLY_SCHEDULER__DAYOFWEEK:
				setDayofweek((DayOfWeekEnum)newValue);
				return;
			case SignalDSLPackage.WEEKLY_SCHEDULER__HOUR:
				setHour((Integer)newValue);
				return;
			case SignalDSLPackage.WEEKLY_SCHEDULER__MINUTE:
				setMinute((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SignalDSLPackage.WEEKLY_SCHEDULER__DAYOFWEEK:
				setDayofweek(DAYOFWEEK_EDEFAULT);
				return;
			case SignalDSLPackage.WEEKLY_SCHEDULER__HOUR:
				setHour(HOUR_EDEFAULT);
				return;
			case SignalDSLPackage.WEEKLY_SCHEDULER__MINUTE:
				setMinute(MINUTE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SignalDSLPackage.WEEKLY_SCHEDULER__DAYOFWEEK:
				return dayofweek != DAYOFWEEK_EDEFAULT;
			case SignalDSLPackage.WEEKLY_SCHEDULER__HOUR:
				return hour != HOUR_EDEFAULT;
			case SignalDSLPackage.WEEKLY_SCHEDULER__MINUTE:
				return minute != MINUTE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (dayofweek: ");
		result.append(dayofweek);
		result.append(", hour: ");
		result.append(hour);
		result.append(", minute: ");
		result.append(minute);
		result.append(')');
		return result.toString();
	}

} //WeeklySchedulerImpl
