/**
 * Copyright (c) 2011, 2018 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0 
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:                                                      
 *     Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 *  
 *  generated from SignalDSL.xcore
 * 
 *  
 */
package org.eclipse.osbp.xtext.signal.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage;

import org.eclipse.osbp.xtext.datainterchange.DataDSLPackage;

import org.eclipse.osbp.xtext.functionlibrarydsl.FunctionLibraryDSLPackage;

import org.eclipse.osbp.xtext.signal.CronScheduler;
import org.eclipse.osbp.xtext.signal.DailyScheduler;
import org.eclipse.osbp.xtext.signal.DayOfWeekEnum;
import org.eclipse.osbp.xtext.signal.HourlyScheduler;
import org.eclipse.osbp.xtext.signal.MonthlyScheduler;
import org.eclipse.osbp.xtext.signal.SchedulerType;
import org.eclipse.osbp.xtext.signal.SignalActionTypeEnum;
import org.eclipse.osbp.xtext.signal.SignalDSLFactory;
import org.eclipse.osbp.xtext.signal.SignalDSLPackage;
import org.eclipse.osbp.xtext.signal.SignalDatainterchange;
import org.eclipse.osbp.xtext.signal.SignalDefinition;
import org.eclipse.osbp.xtext.signal.SignalExecutionTypeEnum;
import org.eclipse.osbp.xtext.signal.SignalFunction;
import org.eclipse.osbp.xtext.signal.SignalLazyResolver;
import org.eclipse.osbp.xtext.signal.SignalModel;
import org.eclipse.osbp.xtext.signal.SignalPackage;
import org.eclipse.osbp.xtext.signal.SignalScheduler;
import org.eclipse.osbp.xtext.signal.SignalTask;
import org.eclipse.osbp.xtext.signal.SignalWatcher;
import org.eclipse.osbp.xtext.signal.WeeklyScheduler;

import org.eclipse.xtext.xtype.XtypePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class SignalDSLPackageImpl extends EPackageImpl implements SignalDSLPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass signalModelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass signalLazyResolverEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass signalPackageEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass signalDefinitionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass signalWatcherEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass signalSchedulerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass schedulerTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass cronSchedulerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass hourlySchedulerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dailySchedulerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass weeklySchedulerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass monthlySchedulerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass signalTaskEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass signalDatainterchangeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass signalFunctionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum signalActionTypeEnumEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum signalExecutionTypeEnumEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum dayOfWeekEnumEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType internalEObjectEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.eclipse.osbp.xtext.signal.SignalDSLPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private SignalDSLPackageImpl() {
		super(eNS_URI, SignalDSLFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link SignalDSLPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static SignalDSLPackage init() {
		if (isInited) return (SignalDSLPackage)EPackage.Registry.INSTANCE.getEPackage(SignalDSLPackage.eNS_URI);

		// Obtain or create and register package
		SignalDSLPackageImpl theSignalDSLPackage = (SignalDSLPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof SignalDSLPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new SignalDSLPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		DataDSLPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theSignalDSLPackage.createPackageContents();

		// Initialize created meta-data
		theSignalDSLPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theSignalDSLPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(SignalDSLPackage.eNS_URI, theSignalDSLPackage);
		return theSignalDSLPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSignalModel() {
		return signalModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSignalModel_ImportSection() {
		return (EReference)signalModelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSignalModel_Packages() {
		return (EReference)signalModelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSignalLazyResolver() {
		return signalLazyResolverEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getSignalLazyResolver__EResolveProxy__InternalEObject() {
		return signalLazyResolverEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSignalPackage() {
		return signalPackageEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSignalPackage_Signals() {
		return (EReference)signalPackageEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSignalDefinition() {
		return signalDefinitionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSignalDefinition_Name() {
		return (EAttribute)signalDefinitionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSignalDefinition_Interchangegroup() {
		return (EReference)signalDefinitionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSignalDefinition_Executiontype() {
		return (EAttribute)signalDefinitionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSignalDefinition_Tasks() {
		return (EReference)signalDefinitionEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSignalWatcher() {
		return signalWatcherEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSignalWatcher_DefaultInterchange() {
		return (EReference)signalWatcherEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSignalWatcher_HasFileMask() {
		return (EAttribute)signalWatcherEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSignalWatcher_Identifier() {
		return (EAttribute)signalWatcherEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSignalScheduler() {
		return signalSchedulerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSignalScheduler_Schedulertype() {
		return (EReference)signalSchedulerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSchedulerType() {
		return schedulerTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCronScheduler() {
		return cronSchedulerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCronScheduler_Expression() {
		return (EAttribute)cronSchedulerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHourlyScheduler() {
		return hourlySchedulerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getHourlyScheduler_Minute() {
		return (EAttribute)hourlySchedulerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDailyScheduler() {
		return dailySchedulerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDailyScheduler_Hour() {
		return (EAttribute)dailySchedulerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDailyScheduler_Minute() {
		return (EAttribute)dailySchedulerEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getWeeklyScheduler() {
		return weeklySchedulerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getWeeklyScheduler_Dayofweek() {
		return (EAttribute)weeklySchedulerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getWeeklyScheduler_Hour() {
		return (EAttribute)weeklySchedulerEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getWeeklyScheduler_Minute() {
		return (EAttribute)weeklySchedulerEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMonthlyScheduler() {
		return monthlySchedulerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMonthlyScheduler_Dayofmonth() {
		return (EAttribute)monthlySchedulerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMonthlyScheduler_Hour() {
		return (EAttribute)monthlySchedulerEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMonthlyScheduler_Minute() {
		return (EAttribute)monthlySchedulerEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSignalTask() {
		return signalTaskEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSignalDatainterchange() {
		return signalDatainterchangeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSignalDatainterchange_ActionType() {
		return (EAttribute)signalDatainterchangeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSignalDatainterchange_DataRef() {
		return (EReference)signalDatainterchangeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSignalDatainterchange_Baseinterchange() {
		return (EAttribute)signalDatainterchangeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSignalFunction() {
		return signalFunctionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSignalFunction_Group() {
		return (EReference)signalFunctionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSignalFunction_DoExecuteFunction() {
		return (EReference)signalFunctionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSignalFunction_OnImportFile() {
		return (EAttribute)signalFunctionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSignalFunction_OnExportFile() {
		return (EAttribute)signalFunctionEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSignalFunction_SupportInterchange() {
		return (EReference)signalFunctionEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getSignalActionTypeEnum() {
		return signalActionTypeEnumEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getSignalExecutionTypeEnum() {
		return signalExecutionTypeEnumEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getDayOfWeekEnum() {
		return dayOfWeekEnumEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getInternalEObject() {
		return internalEObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SignalDSLFactory getSignalDSLFactory() {
		return (SignalDSLFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		signalModelEClass = createEClass(SIGNAL_MODEL);
		createEReference(signalModelEClass, SIGNAL_MODEL__IMPORT_SECTION);
		createEReference(signalModelEClass, SIGNAL_MODEL__PACKAGES);

		signalLazyResolverEClass = createEClass(SIGNAL_LAZY_RESOLVER);
		createEOperation(signalLazyResolverEClass, SIGNAL_LAZY_RESOLVER___ERESOLVE_PROXY__INTERNALEOBJECT);

		signalPackageEClass = createEClass(SIGNAL_PACKAGE);
		createEReference(signalPackageEClass, SIGNAL_PACKAGE__SIGNALS);

		signalDefinitionEClass = createEClass(SIGNAL_DEFINITION);
		createEAttribute(signalDefinitionEClass, SIGNAL_DEFINITION__NAME);
		createEReference(signalDefinitionEClass, SIGNAL_DEFINITION__INTERCHANGEGROUP);
		createEAttribute(signalDefinitionEClass, SIGNAL_DEFINITION__EXECUTIONTYPE);
		createEReference(signalDefinitionEClass, SIGNAL_DEFINITION__TASKS);

		signalWatcherEClass = createEClass(SIGNAL_WATCHER);
		createEReference(signalWatcherEClass, SIGNAL_WATCHER__DEFAULT_INTERCHANGE);
		createEAttribute(signalWatcherEClass, SIGNAL_WATCHER__HAS_FILE_MASK);
		createEAttribute(signalWatcherEClass, SIGNAL_WATCHER__IDENTIFIER);

		signalSchedulerEClass = createEClass(SIGNAL_SCHEDULER);
		createEReference(signalSchedulerEClass, SIGNAL_SCHEDULER__SCHEDULERTYPE);

		schedulerTypeEClass = createEClass(SCHEDULER_TYPE);

		cronSchedulerEClass = createEClass(CRON_SCHEDULER);
		createEAttribute(cronSchedulerEClass, CRON_SCHEDULER__EXPRESSION);

		hourlySchedulerEClass = createEClass(HOURLY_SCHEDULER);
		createEAttribute(hourlySchedulerEClass, HOURLY_SCHEDULER__MINUTE);

		dailySchedulerEClass = createEClass(DAILY_SCHEDULER);
		createEAttribute(dailySchedulerEClass, DAILY_SCHEDULER__HOUR);
		createEAttribute(dailySchedulerEClass, DAILY_SCHEDULER__MINUTE);

		weeklySchedulerEClass = createEClass(WEEKLY_SCHEDULER);
		createEAttribute(weeklySchedulerEClass, WEEKLY_SCHEDULER__DAYOFWEEK);
		createEAttribute(weeklySchedulerEClass, WEEKLY_SCHEDULER__HOUR);
		createEAttribute(weeklySchedulerEClass, WEEKLY_SCHEDULER__MINUTE);

		monthlySchedulerEClass = createEClass(MONTHLY_SCHEDULER);
		createEAttribute(monthlySchedulerEClass, MONTHLY_SCHEDULER__DAYOFMONTH);
		createEAttribute(monthlySchedulerEClass, MONTHLY_SCHEDULER__HOUR);
		createEAttribute(monthlySchedulerEClass, MONTHLY_SCHEDULER__MINUTE);

		signalTaskEClass = createEClass(SIGNAL_TASK);

		signalDatainterchangeEClass = createEClass(SIGNAL_DATAINTERCHANGE);
		createEAttribute(signalDatainterchangeEClass, SIGNAL_DATAINTERCHANGE__ACTION_TYPE);
		createEReference(signalDatainterchangeEClass, SIGNAL_DATAINTERCHANGE__DATA_REF);
		createEAttribute(signalDatainterchangeEClass, SIGNAL_DATAINTERCHANGE__BASEINTERCHANGE);

		signalFunctionEClass = createEClass(SIGNAL_FUNCTION);
		createEReference(signalFunctionEClass, SIGNAL_FUNCTION__GROUP);
		createEReference(signalFunctionEClass, SIGNAL_FUNCTION__DO_EXECUTE_FUNCTION);
		createEAttribute(signalFunctionEClass, SIGNAL_FUNCTION__ON_IMPORT_FILE);
		createEAttribute(signalFunctionEClass, SIGNAL_FUNCTION__ON_EXPORT_FILE);
		createEReference(signalFunctionEClass, SIGNAL_FUNCTION__SUPPORT_INTERCHANGE);

		// Create enums
		signalActionTypeEnumEEnum = createEEnum(SIGNAL_ACTION_TYPE_ENUM);
		signalExecutionTypeEnumEEnum = createEEnum(SIGNAL_EXECUTION_TYPE_ENUM);
		dayOfWeekEnumEEnum = createEEnum(DAY_OF_WEEK_ENUM);

		// Create data types
		internalEObjectEDataType = createEDataType(INTERNAL_EOBJECT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		XtypePackage theXtypePackage = (XtypePackage)EPackage.Registry.INSTANCE.getEPackage(XtypePackage.eNS_URI);
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);
		OSBPTypesPackage theOSBPTypesPackage = (OSBPTypesPackage)EPackage.Registry.INSTANCE.getEPackage(OSBPTypesPackage.eNS_URI);
		DataDSLPackage theDataDSLPackage = (DataDSLPackage)EPackage.Registry.INSTANCE.getEPackage(DataDSLPackage.eNS_URI);
		FunctionLibraryDSLPackage theFunctionLibraryDSLPackage = (FunctionLibraryDSLPackage)EPackage.Registry.INSTANCE.getEPackage(FunctionLibraryDSLPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		signalPackageEClass.getESuperTypes().add(theOSBPTypesPackage.getLPackage());
		signalDefinitionEClass.getESuperTypes().add(this.getSignalLazyResolver());
		signalWatcherEClass.getESuperTypes().add(this.getSignalDefinition());
		signalSchedulerEClass.getESuperTypes().add(this.getSignalDefinition());
		schedulerTypeEClass.getESuperTypes().add(this.getSignalLazyResolver());
		cronSchedulerEClass.getESuperTypes().add(this.getSchedulerType());
		hourlySchedulerEClass.getESuperTypes().add(this.getSchedulerType());
		dailySchedulerEClass.getESuperTypes().add(this.getSchedulerType());
		weeklySchedulerEClass.getESuperTypes().add(this.getSchedulerType());
		monthlySchedulerEClass.getESuperTypes().add(this.getSchedulerType());
		signalTaskEClass.getESuperTypes().add(this.getSignalLazyResolver());
		signalDatainterchangeEClass.getESuperTypes().add(this.getSignalTask());
		signalFunctionEClass.getESuperTypes().add(this.getSignalTask());

		// Initialize classes, features, and operations; add parameters
		initEClass(signalModelEClass, SignalModel.class, "SignalModel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSignalModel_ImportSection(), theXtypePackage.getXImportSection(), null, "importSection", null, 0, 1, SignalModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSignalModel_Packages(), this.getSignalPackage(), null, "packages", null, 0, 1, SignalModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(signalLazyResolverEClass, SignalLazyResolver.class, "SignalLazyResolver", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		EOperation op = initEOperation(getSignalLazyResolver__EResolveProxy__InternalEObject(), theEcorePackage.getEObject(), "eResolveProxy", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getInternalEObject(), "proxy", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(signalPackageEClass, SignalPackage.class, "SignalPackage", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSignalPackage_Signals(), this.getSignalDefinition(), null, "signals", null, 0, -1, SignalPackage.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(signalDefinitionEClass, SignalDefinition.class, "SignalDefinition", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSignalDefinition_Name(), theEcorePackage.getEString(), "name", null, 0, 1, SignalDefinition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSignalDefinition_Interchangegroup(), theDataDSLPackage.getDataInterchangeGroup(), null, "interchangegroup", null, 0, 1, SignalDefinition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSignalDefinition_Executiontype(), this.getSignalExecutionTypeEnum(), "executiontype", null, 0, 1, SignalDefinition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSignalDefinition_Tasks(), this.getSignalTask(), null, "tasks", null, 0, -1, SignalDefinition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(signalWatcherEClass, SignalWatcher.class, "SignalWatcher", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSignalWatcher_DefaultInterchange(), theDataDSLPackage.getDataInterchange(), null, "defaultInterchange", null, 0, 1, SignalWatcher.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSignalWatcher_HasFileMask(), theEcorePackage.getEBoolean(), "hasFileMask", null, 0, 1, SignalWatcher.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSignalWatcher_Identifier(), theEcorePackage.getEString(), "identifier", null, 0, 1, SignalWatcher.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(signalSchedulerEClass, SignalScheduler.class, "SignalScheduler", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSignalScheduler_Schedulertype(), this.getSchedulerType(), null, "schedulertype", null, 0, 1, SignalScheduler.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(schedulerTypeEClass, SchedulerType.class, "SchedulerType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(cronSchedulerEClass, CronScheduler.class, "CronScheduler", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCronScheduler_Expression(), theEcorePackage.getEString(), "expression", null, 0, 1, CronScheduler.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(hourlySchedulerEClass, HourlyScheduler.class, "HourlyScheduler", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getHourlyScheduler_Minute(), theEcorePackage.getEInt(), "minute", null, 0, 1, HourlyScheduler.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dailySchedulerEClass, DailyScheduler.class, "DailyScheduler", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDailyScheduler_Hour(), theEcorePackage.getEInt(), "hour", null, 0, 1, DailyScheduler.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDailyScheduler_Minute(), theEcorePackage.getEInt(), "minute", null, 0, 1, DailyScheduler.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(weeklySchedulerEClass, WeeklyScheduler.class, "WeeklyScheduler", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getWeeklyScheduler_Dayofweek(), this.getDayOfWeekEnum(), "dayofweek", null, 0, 1, WeeklyScheduler.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getWeeklyScheduler_Hour(), theEcorePackage.getEInt(), "hour", null, 0, 1, WeeklyScheduler.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getWeeklyScheduler_Minute(), theEcorePackage.getEInt(), "minute", null, 0, 1, WeeklyScheduler.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(monthlySchedulerEClass, MonthlyScheduler.class, "MonthlyScheduler", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMonthlyScheduler_Dayofmonth(), theEcorePackage.getEInt(), "dayofmonth", null, 0, 1, MonthlyScheduler.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMonthlyScheduler_Hour(), theEcorePackage.getEInt(), "hour", null, 0, 1, MonthlyScheduler.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMonthlyScheduler_Minute(), theEcorePackage.getEInt(), "minute", null, 0, 1, MonthlyScheduler.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(signalTaskEClass, SignalTask.class, "SignalTask", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(signalDatainterchangeEClass, SignalDatainterchange.class, "SignalDatainterchange", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSignalDatainterchange_ActionType(), this.getSignalActionTypeEnum(), "actionType", null, 0, 1, SignalDatainterchange.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSignalDatainterchange_DataRef(), theDataDSLPackage.getDataInterchange(), null, "dataRef", null, 0, 1, SignalDatainterchange.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSignalDatainterchange_Baseinterchange(), theEcorePackage.getEBoolean(), "baseinterchange", null, 0, 1, SignalDatainterchange.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(signalFunctionEClass, SignalFunction.class, "SignalFunction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSignalFunction_Group(), theFunctionLibraryDSLPackage.getFunctionLibraryActionGroup(), null, "group", null, 0, 1, SignalFunction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSignalFunction_DoExecuteFunction(), theFunctionLibraryDSLPackage.getFunctionLibraryExecute(), null, "doExecuteFunction", null, 0, 1, SignalFunction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSignalFunction_OnImportFile(), theEcorePackage.getEBoolean(), "onImportFile", null, 0, 1, SignalFunction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSignalFunction_OnExportFile(), theEcorePackage.getEBoolean(), "onExportFile", null, 0, 1, SignalFunction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSignalFunction_SupportInterchange(), theDataDSLPackage.getDataInterchange(), null, "supportInterchange", null, 0, 1, SignalFunction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(signalActionTypeEnumEEnum, SignalActionTypeEnum.class, "SignalActionTypeEnum");
		addEEnumLiteral(signalActionTypeEnumEEnum, SignalActionTypeEnum.DATAIMPORT);
		addEEnumLiteral(signalActionTypeEnumEEnum, SignalActionTypeEnum.DATAEXPORT);

		initEEnum(signalExecutionTypeEnumEEnum, SignalExecutionTypeEnum.class, "SignalExecutionTypeEnum");
		addEEnumLiteral(signalExecutionTypeEnumEEnum, SignalExecutionTypeEnum.SEQ);
		addEEnumLiteral(signalExecutionTypeEnumEEnum, SignalExecutionTypeEnum.NOSEQ);

		initEEnum(dayOfWeekEnumEEnum, DayOfWeekEnum.class, "DayOfWeekEnum");
		addEEnumLiteral(dayOfWeekEnumEEnum, DayOfWeekEnum.SUNDAY);
		addEEnumLiteral(dayOfWeekEnumEEnum, DayOfWeekEnum.MONDAY);
		addEEnumLiteral(dayOfWeekEnumEEnum, DayOfWeekEnum.TUESDAY);
		addEEnumLiteral(dayOfWeekEnumEEnum, DayOfWeekEnum.WEDNESDAY);
		addEEnumLiteral(dayOfWeekEnumEEnum, DayOfWeekEnum.THURSDAY);
		addEEnumLiteral(dayOfWeekEnumEEnum, DayOfWeekEnum.FRIDAY);
		addEEnumLiteral(dayOfWeekEnumEEnum, DayOfWeekEnum.SATURDAY);

		// Initialize data types
		initEDataType(internalEObjectEDataType, InternalEObject.class, "InternalEObject", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.eclipse.org/emf/2002/Ecore
		createEcoreAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/Ecore</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createEcoreAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/Ecore";	
		addAnnotation
		  (this, 
		   source, 
		   new String[] {
			 "rootPackage", "signaldsl"
		   });
	}

} //SignalDSLPackageImpl
