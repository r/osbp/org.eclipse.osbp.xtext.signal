/**
 * Copyright (c) 2011, 2018 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0 
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:                                                      
 *     Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 *  
 *  generated from SignalDSL.xcore
 * 
 *  
 */
package org.eclipse.osbp.xtext.signal;

import org.eclipse.osbp.xtext.datainterchange.DataInterchange;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Signal Datainterchange</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.signal.SignalDatainterchange#getActionType <em>Action Type</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.signal.SignalDatainterchange#getDataRef <em>Data Ref</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.signal.SignalDatainterchange#isBaseinterchange <em>Baseinterchange</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.xtext.signal.SignalDSLPackage#getSignalDatainterchange()
 * @model
 * @generated
 */
public interface SignalDatainterchange extends SignalTask {
	/**
	 * Returns the value of the '<em><b>Action Type</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.osbp.xtext.signal.SignalActionTypeEnum}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Action Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Action Type</em>' attribute.
	 * @see org.eclipse.osbp.xtext.signal.SignalActionTypeEnum
	 * @see #setActionType(SignalActionTypeEnum)
	 * @see org.eclipse.osbp.xtext.signal.SignalDSLPackage#getSignalDatainterchange_ActionType()
	 * @model unique="false"
	 * @generated
	 */
	SignalActionTypeEnum getActionType();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.signal.SignalDatainterchange#getActionType <em>Action Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Action Type</em>' attribute.
	 * @see org.eclipse.osbp.xtext.signal.SignalActionTypeEnum
	 * @see #getActionType()
	 * @generated
	 */
	void setActionType(SignalActionTypeEnum value);

	/**
	 * Returns the value of the '<em><b>Data Ref</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Ref</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Ref</em>' reference.
	 * @see #setDataRef(DataInterchange)
	 * @see org.eclipse.osbp.xtext.signal.SignalDSLPackage#getSignalDatainterchange_DataRef()
	 * @model
	 * @generated
	 */
	DataInterchange getDataRef();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.signal.SignalDatainterchange#getDataRef <em>Data Ref</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data Ref</em>' reference.
	 * @see #getDataRef()
	 * @generated
	 */
	void setDataRef(DataInterchange value);

	/**
	 * Returns the value of the '<em><b>Baseinterchange</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Baseinterchange</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Baseinterchange</em>' attribute.
	 * @see #setBaseinterchange(boolean)
	 * @see org.eclipse.osbp.xtext.signal.SignalDSLPackage#getSignalDatainterchange_Baseinterchange()
	 * @model unique="false"
	 * @generated
	 */
	boolean isBaseinterchange();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.signal.SignalDatainterchange#isBaseinterchange <em>Baseinterchange</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Baseinterchange</em>' attribute.
	 * @see #isBaseinterchange()
	 * @generated
	 */
	void setBaseinterchange(boolean value);

} // SignalDatainterchange
