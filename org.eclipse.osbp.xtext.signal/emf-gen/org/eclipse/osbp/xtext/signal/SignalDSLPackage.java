/**
 * Copyright (c) 2011, 2018 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0 
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:                                                      
 *     Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 *  
 *  generated from SignalDSL.xcore
 * 
 *  
 */
package org.eclipse.osbp.xtext.signal;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.osbp.dsl.semantic.common.types.OSBPTypesPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.eclipse.osbp.xtext.signal.SignalDSLFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/GenModel fileExtensions='signal' modelName='SignalDSL' prefix='SignalDSL' loadInitialization='false' literalsInterface='true' copyrightText='Copyright (c) 2011, 2018 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)\n All rights reserved. This program and the accompanying materials\n are made available under the terms of the Eclipse Public License 2.0 \n which accompanies this distribution, and is available at\n https://www.eclipse.org/legal/epl-2.0/\n\n SPDX-License-Identifier: EPL-2.0\n \n Contributors:                                                      \n    Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation\n \n generated from SignalDSL.xcore\n\n ' updateClasspath='false' basePackage='org.eclipse.osbp.xtext'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore rootPackage='signaldsl'"
 * @generated
 */
public interface SignalDSLPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "signal";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://osbp.eclipse.org/xtext/signal/SignalDSL";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "signaldsl";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	SignalDSLPackage eINSTANCE = org.eclipse.osbp.xtext.signal.impl.SignalDSLPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.signal.impl.SignalModelImpl <em>Signal Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.signal.impl.SignalModelImpl
	 * @see org.eclipse.osbp.xtext.signal.impl.SignalDSLPackageImpl#getSignalModel()
	 * @generated
	 */
	int SIGNAL_MODEL = 0;

	/**
	 * The feature id for the '<em><b>Import Section</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_MODEL__IMPORT_SECTION = 0;

	/**
	 * The feature id for the '<em><b>Packages</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_MODEL__PACKAGES = 1;

	/**
	 * The number of structural features of the '<em>Signal Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_MODEL_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Signal Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_MODEL_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.signal.impl.SignalLazyResolverImpl <em>Signal Lazy Resolver</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.signal.impl.SignalLazyResolverImpl
	 * @see org.eclipse.osbp.xtext.signal.impl.SignalDSLPackageImpl#getSignalLazyResolver()
	 * @generated
	 */
	int SIGNAL_LAZY_RESOLVER = 1;

	/**
	 * The number of structural features of the '<em>Signal Lazy Resolver</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_LAZY_RESOLVER_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_LAZY_RESOLVER___ERESOLVE_PROXY__INTERNALEOBJECT = 0;

	/**
	 * The number of operations of the '<em>Signal Lazy Resolver</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_LAZY_RESOLVER_OPERATION_COUNT = 1;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.signal.impl.SignalPackageImpl <em>Signal Package</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.signal.impl.SignalPackageImpl
	 * @see org.eclipse.osbp.xtext.signal.impl.SignalDSLPackageImpl#getSignalPackage()
	 * @generated
	 */
	int SIGNAL_PACKAGE = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_PACKAGE__NAME = OSBPTypesPackage.LPACKAGE__NAME;

	/**
	 * The feature id for the '<em><b>Signals</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_PACKAGE__SIGNALS = OSBPTypesPackage.LPACKAGE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Signal Package</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_PACKAGE_FEATURE_COUNT = OSBPTypesPackage.LPACKAGE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_PACKAGE___ERESOLVE_PROXY__INTERNALEOBJECT = OSBPTypesPackage.LPACKAGE___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>Signal Package</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_PACKAGE_OPERATION_COUNT = OSBPTypesPackage.LPACKAGE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.signal.impl.SignalDefinitionImpl <em>Signal Definition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.signal.impl.SignalDefinitionImpl
	 * @see org.eclipse.osbp.xtext.signal.impl.SignalDSLPackageImpl#getSignalDefinition()
	 * @generated
	 */
	int SIGNAL_DEFINITION = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_DEFINITION__NAME = SIGNAL_LAZY_RESOLVER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Interchangegroup</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_DEFINITION__INTERCHANGEGROUP = SIGNAL_LAZY_RESOLVER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Executiontype</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_DEFINITION__EXECUTIONTYPE = SIGNAL_LAZY_RESOLVER_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Tasks</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_DEFINITION__TASKS = SIGNAL_LAZY_RESOLVER_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Signal Definition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_DEFINITION_FEATURE_COUNT = SIGNAL_LAZY_RESOLVER_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_DEFINITION___ERESOLVE_PROXY__INTERNALEOBJECT = SIGNAL_LAZY_RESOLVER___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>Signal Definition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_DEFINITION_OPERATION_COUNT = SIGNAL_LAZY_RESOLVER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.signal.impl.SignalWatcherImpl <em>Signal Watcher</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.signal.impl.SignalWatcherImpl
	 * @see org.eclipse.osbp.xtext.signal.impl.SignalDSLPackageImpl#getSignalWatcher()
	 * @generated
	 */
	int SIGNAL_WATCHER = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_WATCHER__NAME = SIGNAL_DEFINITION__NAME;

	/**
	 * The feature id for the '<em><b>Interchangegroup</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_WATCHER__INTERCHANGEGROUP = SIGNAL_DEFINITION__INTERCHANGEGROUP;

	/**
	 * The feature id for the '<em><b>Executiontype</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_WATCHER__EXECUTIONTYPE = SIGNAL_DEFINITION__EXECUTIONTYPE;

	/**
	 * The feature id for the '<em><b>Tasks</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_WATCHER__TASKS = SIGNAL_DEFINITION__TASKS;

	/**
	 * The feature id for the '<em><b>Default Interchange</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_WATCHER__DEFAULT_INTERCHANGE = SIGNAL_DEFINITION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Has File Mask</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_WATCHER__HAS_FILE_MASK = SIGNAL_DEFINITION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Identifier</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_WATCHER__IDENTIFIER = SIGNAL_DEFINITION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Signal Watcher</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_WATCHER_FEATURE_COUNT = SIGNAL_DEFINITION_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_WATCHER___ERESOLVE_PROXY__INTERNALEOBJECT = SIGNAL_DEFINITION___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>Signal Watcher</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_WATCHER_OPERATION_COUNT = SIGNAL_DEFINITION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.signal.impl.SignalSchedulerImpl <em>Signal Scheduler</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.signal.impl.SignalSchedulerImpl
	 * @see org.eclipse.osbp.xtext.signal.impl.SignalDSLPackageImpl#getSignalScheduler()
	 * @generated
	 */
	int SIGNAL_SCHEDULER = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_SCHEDULER__NAME = SIGNAL_DEFINITION__NAME;

	/**
	 * The feature id for the '<em><b>Interchangegroup</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_SCHEDULER__INTERCHANGEGROUP = SIGNAL_DEFINITION__INTERCHANGEGROUP;

	/**
	 * The feature id for the '<em><b>Executiontype</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_SCHEDULER__EXECUTIONTYPE = SIGNAL_DEFINITION__EXECUTIONTYPE;

	/**
	 * The feature id for the '<em><b>Tasks</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_SCHEDULER__TASKS = SIGNAL_DEFINITION__TASKS;

	/**
	 * The feature id for the '<em><b>Schedulertype</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_SCHEDULER__SCHEDULERTYPE = SIGNAL_DEFINITION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Signal Scheduler</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_SCHEDULER_FEATURE_COUNT = SIGNAL_DEFINITION_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_SCHEDULER___ERESOLVE_PROXY__INTERNALEOBJECT = SIGNAL_DEFINITION___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>Signal Scheduler</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_SCHEDULER_OPERATION_COUNT = SIGNAL_DEFINITION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.signal.impl.SchedulerTypeImpl <em>Scheduler Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.signal.impl.SchedulerTypeImpl
	 * @see org.eclipse.osbp.xtext.signal.impl.SignalDSLPackageImpl#getSchedulerType()
	 * @generated
	 */
	int SCHEDULER_TYPE = 6;

	/**
	 * The number of structural features of the '<em>Scheduler Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULER_TYPE_FEATURE_COUNT = SIGNAL_LAZY_RESOLVER_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULER_TYPE___ERESOLVE_PROXY__INTERNALEOBJECT = SIGNAL_LAZY_RESOLVER___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>Scheduler Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULER_TYPE_OPERATION_COUNT = SIGNAL_LAZY_RESOLVER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.signal.impl.CronSchedulerImpl <em>Cron Scheduler</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.signal.impl.CronSchedulerImpl
	 * @see org.eclipse.osbp.xtext.signal.impl.SignalDSLPackageImpl#getCronScheduler()
	 * @generated
	 */
	int CRON_SCHEDULER = 7;

	/**
	 * The feature id for the '<em><b>Expression</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CRON_SCHEDULER__EXPRESSION = SCHEDULER_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Cron Scheduler</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CRON_SCHEDULER_FEATURE_COUNT = SCHEDULER_TYPE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CRON_SCHEDULER___ERESOLVE_PROXY__INTERNALEOBJECT = SCHEDULER_TYPE___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>Cron Scheduler</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CRON_SCHEDULER_OPERATION_COUNT = SCHEDULER_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.signal.impl.HourlySchedulerImpl <em>Hourly Scheduler</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.signal.impl.HourlySchedulerImpl
	 * @see org.eclipse.osbp.xtext.signal.impl.SignalDSLPackageImpl#getHourlyScheduler()
	 * @generated
	 */
	int HOURLY_SCHEDULER = 8;

	/**
	 * The feature id for the '<em><b>Minute</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOURLY_SCHEDULER__MINUTE = SCHEDULER_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Hourly Scheduler</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOURLY_SCHEDULER_FEATURE_COUNT = SCHEDULER_TYPE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOURLY_SCHEDULER___ERESOLVE_PROXY__INTERNALEOBJECT = SCHEDULER_TYPE___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>Hourly Scheduler</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOURLY_SCHEDULER_OPERATION_COUNT = SCHEDULER_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.signal.impl.DailySchedulerImpl <em>Daily Scheduler</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.signal.impl.DailySchedulerImpl
	 * @see org.eclipse.osbp.xtext.signal.impl.SignalDSLPackageImpl#getDailyScheduler()
	 * @generated
	 */
	int DAILY_SCHEDULER = 9;

	/**
	 * The feature id for the '<em><b>Hour</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAILY_SCHEDULER__HOUR = SCHEDULER_TYPE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Minute</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAILY_SCHEDULER__MINUTE = SCHEDULER_TYPE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Daily Scheduler</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAILY_SCHEDULER_FEATURE_COUNT = SCHEDULER_TYPE_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAILY_SCHEDULER___ERESOLVE_PROXY__INTERNALEOBJECT = SCHEDULER_TYPE___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>Daily Scheduler</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DAILY_SCHEDULER_OPERATION_COUNT = SCHEDULER_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.signal.impl.WeeklySchedulerImpl <em>Weekly Scheduler</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.signal.impl.WeeklySchedulerImpl
	 * @see org.eclipse.osbp.xtext.signal.impl.SignalDSLPackageImpl#getWeeklyScheduler()
	 * @generated
	 */
	int WEEKLY_SCHEDULER = 10;

	/**
	 * The feature id for the '<em><b>Dayofweek</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WEEKLY_SCHEDULER__DAYOFWEEK = SCHEDULER_TYPE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Hour</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WEEKLY_SCHEDULER__HOUR = SCHEDULER_TYPE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Minute</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WEEKLY_SCHEDULER__MINUTE = SCHEDULER_TYPE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Weekly Scheduler</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WEEKLY_SCHEDULER_FEATURE_COUNT = SCHEDULER_TYPE_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WEEKLY_SCHEDULER___ERESOLVE_PROXY__INTERNALEOBJECT = SCHEDULER_TYPE___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>Weekly Scheduler</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WEEKLY_SCHEDULER_OPERATION_COUNT = SCHEDULER_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.signal.impl.MonthlySchedulerImpl <em>Monthly Scheduler</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.signal.impl.MonthlySchedulerImpl
	 * @see org.eclipse.osbp.xtext.signal.impl.SignalDSLPackageImpl#getMonthlyScheduler()
	 * @generated
	 */
	int MONTHLY_SCHEDULER = 11;

	/**
	 * The feature id for the '<em><b>Dayofmonth</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONTHLY_SCHEDULER__DAYOFMONTH = SCHEDULER_TYPE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Hour</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONTHLY_SCHEDULER__HOUR = SCHEDULER_TYPE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Minute</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONTHLY_SCHEDULER__MINUTE = SCHEDULER_TYPE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Monthly Scheduler</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONTHLY_SCHEDULER_FEATURE_COUNT = SCHEDULER_TYPE_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONTHLY_SCHEDULER___ERESOLVE_PROXY__INTERNALEOBJECT = SCHEDULER_TYPE___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>Monthly Scheduler</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONTHLY_SCHEDULER_OPERATION_COUNT = SCHEDULER_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.signal.impl.SignalTaskImpl <em>Signal Task</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.signal.impl.SignalTaskImpl
	 * @see org.eclipse.osbp.xtext.signal.impl.SignalDSLPackageImpl#getSignalTask()
	 * @generated
	 */
	int SIGNAL_TASK = 12;

	/**
	 * The number of structural features of the '<em>Signal Task</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_TASK_FEATURE_COUNT = SIGNAL_LAZY_RESOLVER_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_TASK___ERESOLVE_PROXY__INTERNALEOBJECT = SIGNAL_LAZY_RESOLVER___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>Signal Task</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_TASK_OPERATION_COUNT = SIGNAL_LAZY_RESOLVER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.signal.impl.SignalDatainterchangeImpl <em>Signal Datainterchange</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.signal.impl.SignalDatainterchangeImpl
	 * @see org.eclipse.osbp.xtext.signal.impl.SignalDSLPackageImpl#getSignalDatainterchange()
	 * @generated
	 */
	int SIGNAL_DATAINTERCHANGE = 13;

	/**
	 * The feature id for the '<em><b>Action Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_DATAINTERCHANGE__ACTION_TYPE = SIGNAL_TASK_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Data Ref</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_DATAINTERCHANGE__DATA_REF = SIGNAL_TASK_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Baseinterchange</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_DATAINTERCHANGE__BASEINTERCHANGE = SIGNAL_TASK_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Signal Datainterchange</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_DATAINTERCHANGE_FEATURE_COUNT = SIGNAL_TASK_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_DATAINTERCHANGE___ERESOLVE_PROXY__INTERNALEOBJECT = SIGNAL_TASK___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>Signal Datainterchange</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_DATAINTERCHANGE_OPERATION_COUNT = SIGNAL_TASK_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.signal.impl.SignalFunctionImpl <em>Signal Function</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.signal.impl.SignalFunctionImpl
	 * @see org.eclipse.osbp.xtext.signal.impl.SignalDSLPackageImpl#getSignalFunction()
	 * @generated
	 */
	int SIGNAL_FUNCTION = 14;

	/**
	 * The feature id for the '<em><b>Group</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_FUNCTION__GROUP = SIGNAL_TASK_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Do Execute Function</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_FUNCTION__DO_EXECUTE_FUNCTION = SIGNAL_TASK_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>On Import File</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_FUNCTION__ON_IMPORT_FILE = SIGNAL_TASK_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>On Export File</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_FUNCTION__ON_EXPORT_FILE = SIGNAL_TASK_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Support Interchange</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_FUNCTION__SUPPORT_INTERCHANGE = SIGNAL_TASK_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Signal Function</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_FUNCTION_FEATURE_COUNT = SIGNAL_TASK_FEATURE_COUNT + 5;

	/**
	 * The operation id for the '<em>EResolve Proxy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_FUNCTION___ERESOLVE_PROXY__INTERNALEOBJECT = SIGNAL_TASK___ERESOLVE_PROXY__INTERNALEOBJECT;

	/**
	 * The number of operations of the '<em>Signal Function</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_FUNCTION_OPERATION_COUNT = SIGNAL_TASK_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.signal.SignalActionTypeEnum <em>Signal Action Type Enum</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.signal.SignalActionTypeEnum
	 * @see org.eclipse.osbp.xtext.signal.impl.SignalDSLPackageImpl#getSignalActionTypeEnum()
	 * @generated
	 */
	int SIGNAL_ACTION_TYPE_ENUM = 15;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.signal.SignalExecutionTypeEnum <em>Signal Execution Type Enum</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.signal.SignalExecutionTypeEnum
	 * @see org.eclipse.osbp.xtext.signal.impl.SignalDSLPackageImpl#getSignalExecutionTypeEnum()
	 * @generated
	 */
	int SIGNAL_EXECUTION_TYPE_ENUM = 16;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.xtext.signal.DayOfWeekEnum <em>Day Of Week Enum</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.xtext.signal.DayOfWeekEnum
	 * @see org.eclipse.osbp.xtext.signal.impl.SignalDSLPackageImpl#getDayOfWeekEnum()
	 * @generated
	 */
	int DAY_OF_WEEK_ENUM = 17;

	/**
	 * The meta object id for the '<em>Internal EObject</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.InternalEObject
	 * @see org.eclipse.osbp.xtext.signal.impl.SignalDSLPackageImpl#getInternalEObject()
	 * @generated
	 */
	int INTERNAL_EOBJECT = 18;


	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.signal.SignalModel <em>Signal Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Signal Model</em>'.
	 * @see org.eclipse.osbp.xtext.signal.SignalModel
	 * @generated
	 */
	EClass getSignalModel();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.signal.SignalModel#getImportSection <em>Import Section</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Import Section</em>'.
	 * @see org.eclipse.osbp.xtext.signal.SignalModel#getImportSection()
	 * @see #getSignalModel()
	 * @generated
	 */
	EReference getSignalModel_ImportSection();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.signal.SignalModel#getPackages <em>Packages</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Packages</em>'.
	 * @see org.eclipse.osbp.xtext.signal.SignalModel#getPackages()
	 * @see #getSignalModel()
	 * @generated
	 */
	EReference getSignalModel_Packages();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.signal.SignalLazyResolver <em>Signal Lazy Resolver</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Signal Lazy Resolver</em>'.
	 * @see org.eclipse.osbp.xtext.signal.SignalLazyResolver
	 * @generated
	 */
	EClass getSignalLazyResolver();

	/**
	 * Returns the meta object for the '{@link org.eclipse.osbp.xtext.signal.SignalLazyResolver#eResolveProxy(org.eclipse.emf.ecore.InternalEObject) <em>EResolve Proxy</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>EResolve Proxy</em>' operation.
	 * @see org.eclipse.osbp.xtext.signal.SignalLazyResolver#eResolveProxy(org.eclipse.emf.ecore.InternalEObject)
	 * @generated
	 */
	EOperation getSignalLazyResolver__EResolveProxy__InternalEObject();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.signal.SignalPackage <em>Signal Package</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Signal Package</em>'.
	 * @see org.eclipse.osbp.xtext.signal.SignalPackage
	 * @generated
	 */
	EClass getSignalPackage();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.xtext.signal.SignalPackage#getSignals <em>Signals</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Signals</em>'.
	 * @see org.eclipse.osbp.xtext.signal.SignalPackage#getSignals()
	 * @see #getSignalPackage()
	 * @generated
	 */
	EReference getSignalPackage_Signals();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.signal.SignalDefinition <em>Signal Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Signal Definition</em>'.
	 * @see org.eclipse.osbp.xtext.signal.SignalDefinition
	 * @generated
	 */
	EClass getSignalDefinition();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.signal.SignalDefinition#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.osbp.xtext.signal.SignalDefinition#getName()
	 * @see #getSignalDefinition()
	 * @generated
	 */
	EAttribute getSignalDefinition_Name();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.signal.SignalDefinition#getInterchangegroup <em>Interchangegroup</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Interchangegroup</em>'.
	 * @see org.eclipse.osbp.xtext.signal.SignalDefinition#getInterchangegroup()
	 * @see #getSignalDefinition()
	 * @generated
	 */
	EReference getSignalDefinition_Interchangegroup();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.signal.SignalDefinition#getExecutiontype <em>Executiontype</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Executiontype</em>'.
	 * @see org.eclipse.osbp.xtext.signal.SignalDefinition#getExecutiontype()
	 * @see #getSignalDefinition()
	 * @generated
	 */
	EAttribute getSignalDefinition_Executiontype();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.xtext.signal.SignalDefinition#getTasks <em>Tasks</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Tasks</em>'.
	 * @see org.eclipse.osbp.xtext.signal.SignalDefinition#getTasks()
	 * @see #getSignalDefinition()
	 * @generated
	 */
	EReference getSignalDefinition_Tasks();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.signal.SignalWatcher <em>Signal Watcher</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Signal Watcher</em>'.
	 * @see org.eclipse.osbp.xtext.signal.SignalWatcher
	 * @generated
	 */
	EClass getSignalWatcher();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.signal.SignalWatcher#getDefaultInterchange <em>Default Interchange</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Default Interchange</em>'.
	 * @see org.eclipse.osbp.xtext.signal.SignalWatcher#getDefaultInterchange()
	 * @see #getSignalWatcher()
	 * @generated
	 */
	EReference getSignalWatcher_DefaultInterchange();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.signal.SignalWatcher#isHasFileMask <em>Has File Mask</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Has File Mask</em>'.
	 * @see org.eclipse.osbp.xtext.signal.SignalWatcher#isHasFileMask()
	 * @see #getSignalWatcher()
	 * @generated
	 */
	EAttribute getSignalWatcher_HasFileMask();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.signal.SignalWatcher#getIdentifier <em>Identifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Identifier</em>'.
	 * @see org.eclipse.osbp.xtext.signal.SignalWatcher#getIdentifier()
	 * @see #getSignalWatcher()
	 * @generated
	 */
	EAttribute getSignalWatcher_Identifier();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.signal.SignalScheduler <em>Signal Scheduler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Signal Scheduler</em>'.
	 * @see org.eclipse.osbp.xtext.signal.SignalScheduler
	 * @generated
	 */
	EClass getSignalScheduler();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.xtext.signal.SignalScheduler#getSchedulertype <em>Schedulertype</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Schedulertype</em>'.
	 * @see org.eclipse.osbp.xtext.signal.SignalScheduler#getSchedulertype()
	 * @see #getSignalScheduler()
	 * @generated
	 */
	EReference getSignalScheduler_Schedulertype();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.signal.SchedulerType <em>Scheduler Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Scheduler Type</em>'.
	 * @see org.eclipse.osbp.xtext.signal.SchedulerType
	 * @generated
	 */
	EClass getSchedulerType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.signal.CronScheduler <em>Cron Scheduler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Cron Scheduler</em>'.
	 * @see org.eclipse.osbp.xtext.signal.CronScheduler
	 * @generated
	 */
	EClass getCronScheduler();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.signal.CronScheduler#getExpression <em>Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Expression</em>'.
	 * @see org.eclipse.osbp.xtext.signal.CronScheduler#getExpression()
	 * @see #getCronScheduler()
	 * @generated
	 */
	EAttribute getCronScheduler_Expression();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.signal.HourlyScheduler <em>Hourly Scheduler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Hourly Scheduler</em>'.
	 * @see org.eclipse.osbp.xtext.signal.HourlyScheduler
	 * @generated
	 */
	EClass getHourlyScheduler();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.signal.HourlyScheduler#getMinute <em>Minute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Minute</em>'.
	 * @see org.eclipse.osbp.xtext.signal.HourlyScheduler#getMinute()
	 * @see #getHourlyScheduler()
	 * @generated
	 */
	EAttribute getHourlyScheduler_Minute();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.signal.DailyScheduler <em>Daily Scheduler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Daily Scheduler</em>'.
	 * @see org.eclipse.osbp.xtext.signal.DailyScheduler
	 * @generated
	 */
	EClass getDailyScheduler();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.signal.DailyScheduler#getHour <em>Hour</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Hour</em>'.
	 * @see org.eclipse.osbp.xtext.signal.DailyScheduler#getHour()
	 * @see #getDailyScheduler()
	 * @generated
	 */
	EAttribute getDailyScheduler_Hour();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.signal.DailyScheduler#getMinute <em>Minute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Minute</em>'.
	 * @see org.eclipse.osbp.xtext.signal.DailyScheduler#getMinute()
	 * @see #getDailyScheduler()
	 * @generated
	 */
	EAttribute getDailyScheduler_Minute();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.signal.WeeklyScheduler <em>Weekly Scheduler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Weekly Scheduler</em>'.
	 * @see org.eclipse.osbp.xtext.signal.WeeklyScheduler
	 * @generated
	 */
	EClass getWeeklyScheduler();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.signal.WeeklyScheduler#getDayofweek <em>Dayofweek</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Dayofweek</em>'.
	 * @see org.eclipse.osbp.xtext.signal.WeeklyScheduler#getDayofweek()
	 * @see #getWeeklyScheduler()
	 * @generated
	 */
	EAttribute getWeeklyScheduler_Dayofweek();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.signal.WeeklyScheduler#getHour <em>Hour</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Hour</em>'.
	 * @see org.eclipse.osbp.xtext.signal.WeeklyScheduler#getHour()
	 * @see #getWeeklyScheduler()
	 * @generated
	 */
	EAttribute getWeeklyScheduler_Hour();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.signal.WeeklyScheduler#getMinute <em>Minute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Minute</em>'.
	 * @see org.eclipse.osbp.xtext.signal.WeeklyScheduler#getMinute()
	 * @see #getWeeklyScheduler()
	 * @generated
	 */
	EAttribute getWeeklyScheduler_Minute();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.signal.MonthlyScheduler <em>Monthly Scheduler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Monthly Scheduler</em>'.
	 * @see org.eclipse.osbp.xtext.signal.MonthlyScheduler
	 * @generated
	 */
	EClass getMonthlyScheduler();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.signal.MonthlyScheduler#getDayofmonth <em>Dayofmonth</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Dayofmonth</em>'.
	 * @see org.eclipse.osbp.xtext.signal.MonthlyScheduler#getDayofmonth()
	 * @see #getMonthlyScheduler()
	 * @generated
	 */
	EAttribute getMonthlyScheduler_Dayofmonth();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.signal.MonthlyScheduler#getHour <em>Hour</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Hour</em>'.
	 * @see org.eclipse.osbp.xtext.signal.MonthlyScheduler#getHour()
	 * @see #getMonthlyScheduler()
	 * @generated
	 */
	EAttribute getMonthlyScheduler_Hour();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.signal.MonthlyScheduler#getMinute <em>Minute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Minute</em>'.
	 * @see org.eclipse.osbp.xtext.signal.MonthlyScheduler#getMinute()
	 * @see #getMonthlyScheduler()
	 * @generated
	 */
	EAttribute getMonthlyScheduler_Minute();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.signal.SignalTask <em>Signal Task</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Signal Task</em>'.
	 * @see org.eclipse.osbp.xtext.signal.SignalTask
	 * @generated
	 */
	EClass getSignalTask();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.signal.SignalDatainterchange <em>Signal Datainterchange</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Signal Datainterchange</em>'.
	 * @see org.eclipse.osbp.xtext.signal.SignalDatainterchange
	 * @generated
	 */
	EClass getSignalDatainterchange();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.signal.SignalDatainterchange#getActionType <em>Action Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Action Type</em>'.
	 * @see org.eclipse.osbp.xtext.signal.SignalDatainterchange#getActionType()
	 * @see #getSignalDatainterchange()
	 * @generated
	 */
	EAttribute getSignalDatainterchange_ActionType();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.signal.SignalDatainterchange#getDataRef <em>Data Ref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Data Ref</em>'.
	 * @see org.eclipse.osbp.xtext.signal.SignalDatainterchange#getDataRef()
	 * @see #getSignalDatainterchange()
	 * @generated
	 */
	EReference getSignalDatainterchange_DataRef();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.signal.SignalDatainterchange#isBaseinterchange <em>Baseinterchange</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Baseinterchange</em>'.
	 * @see org.eclipse.osbp.xtext.signal.SignalDatainterchange#isBaseinterchange()
	 * @see #getSignalDatainterchange()
	 * @generated
	 */
	EAttribute getSignalDatainterchange_Baseinterchange();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.xtext.signal.SignalFunction <em>Signal Function</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Signal Function</em>'.
	 * @see org.eclipse.osbp.xtext.signal.SignalFunction
	 * @generated
	 */
	EClass getSignalFunction();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.signal.SignalFunction#getGroup <em>Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Group</em>'.
	 * @see org.eclipse.osbp.xtext.signal.SignalFunction#getGroup()
	 * @see #getSignalFunction()
	 * @generated
	 */
	EReference getSignalFunction_Group();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.signal.SignalFunction#getDoExecuteFunction <em>Do Execute Function</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Do Execute Function</em>'.
	 * @see org.eclipse.osbp.xtext.signal.SignalFunction#getDoExecuteFunction()
	 * @see #getSignalFunction()
	 * @generated
	 */
	EReference getSignalFunction_DoExecuteFunction();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.signal.SignalFunction#isOnImportFile <em>On Import File</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>On Import File</em>'.
	 * @see org.eclipse.osbp.xtext.signal.SignalFunction#isOnImportFile()
	 * @see #getSignalFunction()
	 * @generated
	 */
	EAttribute getSignalFunction_OnImportFile();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.xtext.signal.SignalFunction#isOnExportFile <em>On Export File</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>On Export File</em>'.
	 * @see org.eclipse.osbp.xtext.signal.SignalFunction#isOnExportFile()
	 * @see #getSignalFunction()
	 * @generated
	 */
	EAttribute getSignalFunction_OnExportFile();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.xtext.signal.SignalFunction#getSupportInterchange <em>Support Interchange</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Support Interchange</em>'.
	 * @see org.eclipse.osbp.xtext.signal.SignalFunction#getSupportInterchange()
	 * @see #getSignalFunction()
	 * @generated
	 */
	EReference getSignalFunction_SupportInterchange();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.osbp.xtext.signal.SignalActionTypeEnum <em>Signal Action Type Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Signal Action Type Enum</em>'.
	 * @see org.eclipse.osbp.xtext.signal.SignalActionTypeEnum
	 * @generated
	 */
	EEnum getSignalActionTypeEnum();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.osbp.xtext.signal.SignalExecutionTypeEnum <em>Signal Execution Type Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Signal Execution Type Enum</em>'.
	 * @see org.eclipse.osbp.xtext.signal.SignalExecutionTypeEnum
	 * @generated
	 */
	EEnum getSignalExecutionTypeEnum();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.osbp.xtext.signal.DayOfWeekEnum <em>Day Of Week Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Day Of Week Enum</em>'.
	 * @see org.eclipse.osbp.xtext.signal.DayOfWeekEnum
	 * @generated
	 */
	EEnum getDayOfWeekEnum();

	/**
	 * Returns the meta object for data type '{@link org.eclipse.emf.ecore.InternalEObject <em>Internal EObject</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Internal EObject</em>'.
	 * @see org.eclipse.emf.ecore.InternalEObject
	 * @model instanceClass="org.eclipse.emf.ecore.InternalEObject"
	 * @generated
	 */
	EDataType getInternalEObject();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	SignalDSLFactory getSignalDSLFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.signal.impl.SignalModelImpl <em>Signal Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.signal.impl.SignalModelImpl
		 * @see org.eclipse.osbp.xtext.signal.impl.SignalDSLPackageImpl#getSignalModel()
		 * @generated
		 */
		EClass SIGNAL_MODEL = eINSTANCE.getSignalModel();

		/**
		 * The meta object literal for the '<em><b>Import Section</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SIGNAL_MODEL__IMPORT_SECTION = eINSTANCE.getSignalModel_ImportSection();

		/**
		 * The meta object literal for the '<em><b>Packages</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SIGNAL_MODEL__PACKAGES = eINSTANCE.getSignalModel_Packages();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.signal.impl.SignalLazyResolverImpl <em>Signal Lazy Resolver</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.signal.impl.SignalLazyResolverImpl
		 * @see org.eclipse.osbp.xtext.signal.impl.SignalDSLPackageImpl#getSignalLazyResolver()
		 * @generated
		 */
		EClass SIGNAL_LAZY_RESOLVER = eINSTANCE.getSignalLazyResolver();

		/**
		 * The meta object literal for the '<em><b>EResolve Proxy</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SIGNAL_LAZY_RESOLVER___ERESOLVE_PROXY__INTERNALEOBJECT = eINSTANCE.getSignalLazyResolver__EResolveProxy__InternalEObject();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.signal.impl.SignalPackageImpl <em>Signal Package</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.signal.impl.SignalPackageImpl
		 * @see org.eclipse.osbp.xtext.signal.impl.SignalDSLPackageImpl#getSignalPackage()
		 * @generated
		 */
		EClass SIGNAL_PACKAGE = eINSTANCE.getSignalPackage();

		/**
		 * The meta object literal for the '<em><b>Signals</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SIGNAL_PACKAGE__SIGNALS = eINSTANCE.getSignalPackage_Signals();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.signal.impl.SignalDefinitionImpl <em>Signal Definition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.signal.impl.SignalDefinitionImpl
		 * @see org.eclipse.osbp.xtext.signal.impl.SignalDSLPackageImpl#getSignalDefinition()
		 * @generated
		 */
		EClass SIGNAL_DEFINITION = eINSTANCE.getSignalDefinition();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SIGNAL_DEFINITION__NAME = eINSTANCE.getSignalDefinition_Name();

		/**
		 * The meta object literal for the '<em><b>Interchangegroup</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SIGNAL_DEFINITION__INTERCHANGEGROUP = eINSTANCE.getSignalDefinition_Interchangegroup();

		/**
		 * The meta object literal for the '<em><b>Executiontype</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SIGNAL_DEFINITION__EXECUTIONTYPE = eINSTANCE.getSignalDefinition_Executiontype();

		/**
		 * The meta object literal for the '<em><b>Tasks</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SIGNAL_DEFINITION__TASKS = eINSTANCE.getSignalDefinition_Tasks();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.signal.impl.SignalWatcherImpl <em>Signal Watcher</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.signal.impl.SignalWatcherImpl
		 * @see org.eclipse.osbp.xtext.signal.impl.SignalDSLPackageImpl#getSignalWatcher()
		 * @generated
		 */
		EClass SIGNAL_WATCHER = eINSTANCE.getSignalWatcher();

		/**
		 * The meta object literal for the '<em><b>Default Interchange</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SIGNAL_WATCHER__DEFAULT_INTERCHANGE = eINSTANCE.getSignalWatcher_DefaultInterchange();

		/**
		 * The meta object literal for the '<em><b>Has File Mask</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SIGNAL_WATCHER__HAS_FILE_MASK = eINSTANCE.getSignalWatcher_HasFileMask();

		/**
		 * The meta object literal for the '<em><b>Identifier</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SIGNAL_WATCHER__IDENTIFIER = eINSTANCE.getSignalWatcher_Identifier();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.signal.impl.SignalSchedulerImpl <em>Signal Scheduler</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.signal.impl.SignalSchedulerImpl
		 * @see org.eclipse.osbp.xtext.signal.impl.SignalDSLPackageImpl#getSignalScheduler()
		 * @generated
		 */
		EClass SIGNAL_SCHEDULER = eINSTANCE.getSignalScheduler();

		/**
		 * The meta object literal for the '<em><b>Schedulertype</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SIGNAL_SCHEDULER__SCHEDULERTYPE = eINSTANCE.getSignalScheduler_Schedulertype();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.signal.impl.SchedulerTypeImpl <em>Scheduler Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.signal.impl.SchedulerTypeImpl
		 * @see org.eclipse.osbp.xtext.signal.impl.SignalDSLPackageImpl#getSchedulerType()
		 * @generated
		 */
		EClass SCHEDULER_TYPE = eINSTANCE.getSchedulerType();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.signal.impl.CronSchedulerImpl <em>Cron Scheduler</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.signal.impl.CronSchedulerImpl
		 * @see org.eclipse.osbp.xtext.signal.impl.SignalDSLPackageImpl#getCronScheduler()
		 * @generated
		 */
		EClass CRON_SCHEDULER = eINSTANCE.getCronScheduler();

		/**
		 * The meta object literal for the '<em><b>Expression</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CRON_SCHEDULER__EXPRESSION = eINSTANCE.getCronScheduler_Expression();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.signal.impl.HourlySchedulerImpl <em>Hourly Scheduler</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.signal.impl.HourlySchedulerImpl
		 * @see org.eclipse.osbp.xtext.signal.impl.SignalDSLPackageImpl#getHourlyScheduler()
		 * @generated
		 */
		EClass HOURLY_SCHEDULER = eINSTANCE.getHourlyScheduler();

		/**
		 * The meta object literal for the '<em><b>Minute</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HOURLY_SCHEDULER__MINUTE = eINSTANCE.getHourlyScheduler_Minute();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.signal.impl.DailySchedulerImpl <em>Daily Scheduler</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.signal.impl.DailySchedulerImpl
		 * @see org.eclipse.osbp.xtext.signal.impl.SignalDSLPackageImpl#getDailyScheduler()
		 * @generated
		 */
		EClass DAILY_SCHEDULER = eINSTANCE.getDailyScheduler();

		/**
		 * The meta object literal for the '<em><b>Hour</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DAILY_SCHEDULER__HOUR = eINSTANCE.getDailyScheduler_Hour();

		/**
		 * The meta object literal for the '<em><b>Minute</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DAILY_SCHEDULER__MINUTE = eINSTANCE.getDailyScheduler_Minute();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.signal.impl.WeeklySchedulerImpl <em>Weekly Scheduler</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.signal.impl.WeeklySchedulerImpl
		 * @see org.eclipse.osbp.xtext.signal.impl.SignalDSLPackageImpl#getWeeklyScheduler()
		 * @generated
		 */
		EClass WEEKLY_SCHEDULER = eINSTANCE.getWeeklyScheduler();

		/**
		 * The meta object literal for the '<em><b>Dayofweek</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute WEEKLY_SCHEDULER__DAYOFWEEK = eINSTANCE.getWeeklyScheduler_Dayofweek();

		/**
		 * The meta object literal for the '<em><b>Hour</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute WEEKLY_SCHEDULER__HOUR = eINSTANCE.getWeeklyScheduler_Hour();

		/**
		 * The meta object literal for the '<em><b>Minute</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute WEEKLY_SCHEDULER__MINUTE = eINSTANCE.getWeeklyScheduler_Minute();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.signal.impl.MonthlySchedulerImpl <em>Monthly Scheduler</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.signal.impl.MonthlySchedulerImpl
		 * @see org.eclipse.osbp.xtext.signal.impl.SignalDSLPackageImpl#getMonthlyScheduler()
		 * @generated
		 */
		EClass MONTHLY_SCHEDULER = eINSTANCE.getMonthlyScheduler();

		/**
		 * The meta object literal for the '<em><b>Dayofmonth</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MONTHLY_SCHEDULER__DAYOFMONTH = eINSTANCE.getMonthlyScheduler_Dayofmonth();

		/**
		 * The meta object literal for the '<em><b>Hour</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MONTHLY_SCHEDULER__HOUR = eINSTANCE.getMonthlyScheduler_Hour();

		/**
		 * The meta object literal for the '<em><b>Minute</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MONTHLY_SCHEDULER__MINUTE = eINSTANCE.getMonthlyScheduler_Minute();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.signal.impl.SignalTaskImpl <em>Signal Task</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.signal.impl.SignalTaskImpl
		 * @see org.eclipse.osbp.xtext.signal.impl.SignalDSLPackageImpl#getSignalTask()
		 * @generated
		 */
		EClass SIGNAL_TASK = eINSTANCE.getSignalTask();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.signal.impl.SignalDatainterchangeImpl <em>Signal Datainterchange</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.signal.impl.SignalDatainterchangeImpl
		 * @see org.eclipse.osbp.xtext.signal.impl.SignalDSLPackageImpl#getSignalDatainterchange()
		 * @generated
		 */
		EClass SIGNAL_DATAINTERCHANGE = eINSTANCE.getSignalDatainterchange();

		/**
		 * The meta object literal for the '<em><b>Action Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SIGNAL_DATAINTERCHANGE__ACTION_TYPE = eINSTANCE.getSignalDatainterchange_ActionType();

		/**
		 * The meta object literal for the '<em><b>Data Ref</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SIGNAL_DATAINTERCHANGE__DATA_REF = eINSTANCE.getSignalDatainterchange_DataRef();

		/**
		 * The meta object literal for the '<em><b>Baseinterchange</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SIGNAL_DATAINTERCHANGE__BASEINTERCHANGE = eINSTANCE.getSignalDatainterchange_Baseinterchange();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.signal.impl.SignalFunctionImpl <em>Signal Function</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.signal.impl.SignalFunctionImpl
		 * @see org.eclipse.osbp.xtext.signal.impl.SignalDSLPackageImpl#getSignalFunction()
		 * @generated
		 */
		EClass SIGNAL_FUNCTION = eINSTANCE.getSignalFunction();

		/**
		 * The meta object literal for the '<em><b>Group</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SIGNAL_FUNCTION__GROUP = eINSTANCE.getSignalFunction_Group();

		/**
		 * The meta object literal for the '<em><b>Do Execute Function</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SIGNAL_FUNCTION__DO_EXECUTE_FUNCTION = eINSTANCE.getSignalFunction_DoExecuteFunction();

		/**
		 * The meta object literal for the '<em><b>On Import File</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SIGNAL_FUNCTION__ON_IMPORT_FILE = eINSTANCE.getSignalFunction_OnImportFile();

		/**
		 * The meta object literal for the '<em><b>On Export File</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SIGNAL_FUNCTION__ON_EXPORT_FILE = eINSTANCE.getSignalFunction_OnExportFile();

		/**
		 * The meta object literal for the '<em><b>Support Interchange</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SIGNAL_FUNCTION__SUPPORT_INTERCHANGE = eINSTANCE.getSignalFunction_SupportInterchange();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.signal.SignalActionTypeEnum <em>Signal Action Type Enum</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.signal.SignalActionTypeEnum
		 * @see org.eclipse.osbp.xtext.signal.impl.SignalDSLPackageImpl#getSignalActionTypeEnum()
		 * @generated
		 */
		EEnum SIGNAL_ACTION_TYPE_ENUM = eINSTANCE.getSignalActionTypeEnum();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.signal.SignalExecutionTypeEnum <em>Signal Execution Type Enum</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.signal.SignalExecutionTypeEnum
		 * @see org.eclipse.osbp.xtext.signal.impl.SignalDSLPackageImpl#getSignalExecutionTypeEnum()
		 * @generated
		 */
		EEnum SIGNAL_EXECUTION_TYPE_ENUM = eINSTANCE.getSignalExecutionTypeEnum();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.xtext.signal.DayOfWeekEnum <em>Day Of Week Enum</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.xtext.signal.DayOfWeekEnum
		 * @see org.eclipse.osbp.xtext.signal.impl.SignalDSLPackageImpl#getDayOfWeekEnum()
		 * @generated
		 */
		EEnum DAY_OF_WEEK_ENUM = eINSTANCE.getDayOfWeekEnum();

		/**
		 * The meta object literal for the '<em>Internal EObject</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.emf.ecore.InternalEObject
		 * @see org.eclipse.osbp.xtext.signal.impl.SignalDSLPackageImpl#getInternalEObject()
		 * @generated
		 */
		EDataType INTERNAL_EOBJECT = eINSTANCE.getInternalEObject();

	}

} //SignalDSLPackage
