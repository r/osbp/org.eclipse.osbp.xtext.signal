/**
 * Copyright (c) 2011, 2018 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License 2.0 
 *  which accompanies this distribution, and is available at
 *  https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:                                                      
 *     Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 *  
 *  generated from SignalDSL.xcore
 * 
 *  
 */
package org.eclipse.osbp.xtext.signal;

import org.eclipse.emf.common.util.EList;

import org.eclipse.osbp.xtext.datainterchange.DataInterchangeGroup;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Signal Definition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.xtext.signal.SignalDefinition#getName <em>Name</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.signal.SignalDefinition#getInterchangegroup <em>Interchangegroup</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.signal.SignalDefinition#getExecutiontype <em>Executiontype</em>}</li>
 *   <li>{@link org.eclipse.osbp.xtext.signal.SignalDefinition#getTasks <em>Tasks</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.xtext.signal.SignalDSLPackage#getSignalDefinition()
 * @model
 * @generated
 */
public interface SignalDefinition extends SignalLazyResolver {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see org.eclipse.osbp.xtext.signal.SignalDSLPackage#getSignalDefinition_Name()
	 * @model unique="false"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.signal.SignalDefinition#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Interchangegroup</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Interchangegroup</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interchangegroup</em>' reference.
	 * @see #setInterchangegroup(DataInterchangeGroup)
	 * @see org.eclipse.osbp.xtext.signal.SignalDSLPackage#getSignalDefinition_Interchangegroup()
	 * @model
	 * @generated
	 */
	DataInterchangeGroup getInterchangegroup();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.signal.SignalDefinition#getInterchangegroup <em>Interchangegroup</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Interchangegroup</em>' reference.
	 * @see #getInterchangegroup()
	 * @generated
	 */
	void setInterchangegroup(DataInterchangeGroup value);

	/**
	 * Returns the value of the '<em><b>Executiontype</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.osbp.xtext.signal.SignalExecutionTypeEnum}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Executiontype</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Executiontype</em>' attribute.
	 * @see org.eclipse.osbp.xtext.signal.SignalExecutionTypeEnum
	 * @see #setExecutiontype(SignalExecutionTypeEnum)
	 * @see org.eclipse.osbp.xtext.signal.SignalDSLPackage#getSignalDefinition_Executiontype()
	 * @model unique="false"
	 * @generated
	 */
	SignalExecutionTypeEnum getExecutiontype();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.xtext.signal.SignalDefinition#getExecutiontype <em>Executiontype</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Executiontype</em>' attribute.
	 * @see org.eclipse.osbp.xtext.signal.SignalExecutionTypeEnum
	 * @see #getExecutiontype()
	 * @generated
	 */
	void setExecutiontype(SignalExecutionTypeEnum value);

	/**
	 * Returns the value of the '<em><b>Tasks</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.osbp.xtext.signal.SignalTask}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tasks</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tasks</em>' containment reference list.
	 * @see org.eclipse.osbp.xtext.signal.SignalDSLPackage#getSignalDefinition_Tasks()
	 * @model containment="true"
	 * @generated
	 */
	EList<SignalTask> getTasks();

} // SignalDefinition
