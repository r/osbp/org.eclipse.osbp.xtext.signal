/**
 *                                                                            
 *  Copyright (c) 2011, 2018 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 * 
 *  This copyright notice shows up in the generated Java code
 * 
 */

package org.eclipse.osbp.xtext.signal.jvmmodel

import com.google.common.util.concurrent.ThreadFactoryBuilder
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.IOException
import java.io.PrintWriter
import java.io.StringWriter
import java.net.URI
import java.net.URL
import java.nio.file.FileSystems
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.nio.file.WatchEvent
import java.util.ArrayList
import java.util.Date
import java.util.HashMap
import java.util.HashSet
import java.util.Map
import java.util.Properties
import java.util.concurrent.Executors
import javax.inject.Inject
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.util.EcoreUtil
import org.eclipse.osbp.core.api.persistence.IPersistenceService
import org.eclipse.osbp.datainterchange.api.IDataInterchange
import org.eclipse.osbp.preferences.ProductConfiguration
import org.eclipse.osbp.xtext.addons.EObjectHelper
import org.eclipse.osbp.xtext.basic.generator.BasicDslGeneratorUtils
import org.eclipse.osbp.xtext.datainterchange.DataInterchangeGroup
import org.eclipse.osbp.xtext.datainterchange.common.WorkerThreadRunnable
import org.eclipse.osbp.xtext.i18n.I18NModelGenerator
import org.eclipse.osbp.xtext.signal.CronScheduler
import org.eclipse.osbp.xtext.signal.DailyScheduler
import org.eclipse.osbp.xtext.signal.HourlyScheduler
import org.eclipse.osbp.xtext.signal.MonthlyScheduler
import org.eclipse.osbp.xtext.signal.SignalPackage
import org.eclipse.osbp.xtext.signal.SignalScheduler
import org.eclipse.osbp.xtext.signal.SignalWatcher
import org.eclipse.osbp.xtext.signal.WeeklyScheduler
import org.eclipse.osbp.xtext.signal.common.SignalConstants
import org.eclipse.osbp.xtext.signal.common.WatcherImpl
import org.eclipse.xtext.generator.IFileSystemAccess
import org.eclipse.xtext.naming.IQualifiedNameProvider
import org.eclipse.xtext.xbase.compiler.GeneratorConfig
import org.eclipse.xtext.xbase.compiler.ImportManager
import org.quartz.JobDetail
import org.quartz.JobExecutionException
import org.quartz.SchedulerException
import org.quartz.Trigger
import org.quartz.impl.StdSchedulerFactory
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import static org.eclipse.osbp.xtext.signal.common.SignalConstants.*

class SignalModelGenerator extends I18NModelGenerator {
	@Inject extension BasicDslGeneratorUtils
	@Inject extension IQualifiedNameProvider

	override createAppendable(EObject context, ImportManager importManager, GeneratorConfig config) {
		// required to initialize the needed builder to avoid deprecated methods
		builder = context.eResource
		// ---------
		addImportFor(
			importManager,	_typeReferenceBuilder, WatcherImpl,	SignalConstants, Paths, WorkerThreadRunnable, IPersistenceService,IDataInterchange,
			Files, FileSystems, FileOutputStream, Logger, LoggerFactory, HashMap, WatchEvent, Path, URL, Executors, Map, ArrayList, ProductConfiguration,
			File, FileInputStream, Properties, StringWriter, PrintWriter, SchedulerException, IOException, Date, JobDetail, JobExecutionException, Trigger,
			URI, ThreadFactoryBuilder, StdSchedulerFactory
		)
		super.createAppendable(context, importManager, config)
	}

	override doGenerate(Resource input, IFileSystemAccess fsa) {
		EcoreUtil.getAllContents(EObjectHelper.getSemanticElement(input), false).filter(typeof(SignalPackage)).forEach[
			fsa.generatePathConfig(it)
		]
		super.doGenerate(input, fsa)
	}
	
	def void generatePathConfig(IFileSystemAccess access, SignalPackage pck) {
		var dir = new File('''«System.getProperty("user.home")»/.osbee''')
        if(!dir.exists) {
        	dir.mkdir
        }
        
        val dataGroups = new HashSet<DataInterchangeGroup>
        pck.signals.forEach[
        	if(it instanceof SignalWatcher){
	        	dataGroups.add(it.interchangegroup)
       		}
       		else if(it instanceof SignalScheduler){
	        	dataGroups.add(it.interchangegroup)
       		}
        ]
        // making sure that a configuration file is written only once
        // with all corresponding watchers and schedulers config data
        for(group : dataGroups){	
	        var file = new File('''«System.getProperty("user.home")»/.osbee/«group.name»Config.xml''');
	        file.setWritable(true, false);
	        if(file.exists) {
		    	// first read the file content
			    val properties = new Properties();
		    	var fileInput = new FileInputStream(file);
				properties.loadFromXML(fileInput);
				fileInput.close();
			    
		        pck.signals.forEach[
		        	
		        	var tempgroup = null as DataInterchangeGroup 
		       		if(it instanceof SignalWatcher){
		       			tempgroup = it.interchangegroup
		       		}
		       		else if(it instanceof SignalScheduler){
		       			tempgroup = it.interchangegroup
		       		}
		        	
		        	if(group === tempgroup){
		        		
		        		if(it instanceof SignalWatcher){
			        		// maximum thread count for parallel job execution
			        		if(!properties.containsKey(MAXPARALLELTHREADCOUNT_NAME)){
								properties.put(MAXPARALLELTHREADCOUNT_NAME, DEFAULT_PARALLEL_THREADCOUNT.toString)
			        		}
							// watcher config filemask   
							if(it.hasFileMask ){
								if(!properties.containsKey(it.fullyQualifiedName +"Watcher"+ FILEMASK)){									
									properties.put(it.fullyQualifiedName +"Watcher"+FILEMASK, (it.identifier))
								}
							}else { // watcher config filename
								if(!properties.containsKey(it.fullyQualifiedName +"Watcher"+ FILENAME)){
									properties.put(it.fullyQualifiedName +"Watcher"+ FILENAME, it.identifier)
								}
							}
		        		}
						else if (it instanceof SignalScheduler){
							// cron scheduler
							if(it.schedulertype instanceof CronScheduler && !properties.containsKey(it.fullyQualifiedName +"Scheduler"+ CRON_SCHEDULER) ) {
								properties.put(it.fullyQualifiedName +"Scheduler"+ CRON_SCHEDULER,(it.schedulertype as CronScheduler).expression)
							}
							// hourly scheduler
							else if(it.schedulertype instanceof HourlyScheduler && !properties.containsKey(it.fullyQualifiedName +"Scheduler"+ HOURLY_SCHEDULER) ) {
								properties.put(it.fullyQualifiedName +"Scheduler"+ HOURLY_SCHEDULER,(it.schedulertype as HourlyScheduler).minute.toString)
							}
							// daily scheduler
							else if(it.schedulertype instanceof DailyScheduler  && !properties.containsKey(it.fullyQualifiedName +"Scheduler"+ DAILY_SCHEDULER_HOUR) ) {
								properties.put(it.fullyQualifiedName +"Scheduler"+ DAILY_SCHEDULER_HOUR, '''«(it.schedulertype as DailyScheduler).hour»'''.toString)
							}
							else if(it.schedulertype instanceof DailyScheduler  && !properties.containsKey(it.fullyQualifiedName +"Scheduler"+ DAILY_SCHEDULER_MIN) ) {
								properties.put(it.fullyQualifiedName +"Scheduler"+ DAILY_SCHEDULER_MIN, '''«(it.schedulertype as DailyScheduler).minute»'''.toString)
							}
							// weekly scheduler
							else if(it.schedulertype instanceof WeeklyScheduler && !properties.containsKey(it.fullyQualifiedName +"Scheduler"+ WEEKLY_SCHEDULER_DAYOFWEEK) ) {
								properties.put(it.fullyQualifiedName +"Scheduler"+ WEEKLY_SCHEDULER_DAYOFWEEK, '''«(it.schedulertype as WeeklyScheduler).dayofweek»'''.toString)
							}
							else if(it.schedulertype instanceof WeeklyScheduler && !properties.containsKey(it.fullyQualifiedName +"Scheduler"+ WEEKLY_SCHEDULER_HOUR) ) {
								properties.put(it.fullyQualifiedName +"Scheduler"+ WEEKLY_SCHEDULER_HOUR, '''«(it.schedulertype as WeeklyScheduler).hour»'''.toString)
							}
							else if(it.schedulertype instanceof WeeklyScheduler && !properties.containsKey(it.fullyQualifiedName +"Scheduler"+ WEEKLY_SCHEDULER_MIN) ) {
								properties.put(it.fullyQualifiedName +"Scheduler"+ WEEKLY_SCHEDULER_MIN, '''«(it.schedulertype as WeeklyScheduler).minute»'''.toString)
							}
							// monthly scheduler
							else if(it.schedulertype instanceof MonthlyScheduler  && !properties.containsKey(it.fullyQualifiedName +"Scheduler"+ MONTHLY_SCHEDULER_DAYOFMONTH) ) {
								properties.put(it.fullyQualifiedName +"Scheduler"+ MONTHLY_SCHEDULER_DAYOFMONTH, '''«(it.schedulertype as MonthlyScheduler).dayofmonth»'''.toString)
							}
							else if(it.schedulertype instanceof MonthlyScheduler  && !properties.containsKey(it.fullyQualifiedName +"Scheduler"+ MONTHLY_SCHEDULER_HOUR) ) {
								properties.put(it.fullyQualifiedName +"Scheduler"+ MONTHLY_SCHEDULER_HOUR, '''«(it.schedulertype as MonthlyScheduler).hour»'''.toString)
							}
							else if(it.schedulertype instanceof MonthlyScheduler  && !properties.containsKey(it.fullyQualifiedName +"Scheduler"+ MONTHLY_SCHEDULER_MIN) ) {
								properties.put(it.fullyQualifiedName +"Scheduler"+ MONTHLY_SCHEDULER_MIN, '''«(it.schedulertype as MonthlyScheduler).minute»'''.toString)
							}
						}
				    }
		        ]
		        // write the all watcher and scheduler configurations to the file
				var fileOutput = new FileOutputStream(file);
				properties.storeToXML(fileOutput, "dataInterchange file URLs");
				fileOutput.close
        	}
        }
	}
}