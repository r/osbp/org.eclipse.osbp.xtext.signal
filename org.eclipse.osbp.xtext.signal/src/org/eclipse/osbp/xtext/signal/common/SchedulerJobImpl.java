package org.eclipse.osbp.xtext.signal.common;

import java.util.HashSet;

import org.eclipse.osbp.xtext.datainterchange.common.WorkerThreadRunnable;

public class SchedulerJobImpl extends SignalCommonData {
	/** the full scheduler qualified name */
	private String schedulerid;
	/** the task status*/
	private boolean done;
	/** holds the completion status of each task in the parallel case*/
	private HashSet<WorkerThreadRunnable> completionstatus;
	
	/**
	 * Default constructor
	 */
	public SchedulerJobImpl(){
		completionstatus = new HashSet<>();
	}
	
	/**
	 * Triggers the execution of the list of task.
	 * This operation has to be overridden by subclasses.
	 */
	public void executeListOfTasks(){}

	/**
	 * Returns the scheduler full qualifier id.
	 * @return {@link String} the value of {@link #schedulerid}
	 */
	public String getSchedulerId() {
		return schedulerid;
	}

	/**
	 * Sets the scheduler id value {@link #schedulerid}.
	 * @param schedulerid the new value
	 */
	public void setSchedulerId(String schedulerid) {
		this.schedulerid = schedulerid;
	}

	/**
	 * Indicates if the execution of the list of tasks is done or not.
	 * @return true if yes, false if not.
	 */
	public boolean isDone() {
		return done;
	}

	/**
	 * Sets the state of scheduler task: done or not.
	 * @param done the state value
	 */
	public void setDone(boolean done) {
		this.done = done;
	}

	/**
	 * Adds an (interchange) task to the list of all tasks,
	 * in order to monitor when it's done.
	 * @param task the {@link WorkerThreadRunnable} task
	 */
	public void checkForCompletion(WorkerThreadRunnable task) {
		completionstatus.add(task);
	}
	
	/**
	 * Updates the state of this scheduler task.
	 */
	public void updateTaskCompletionState(){
		boolean result = true;
		for(WorkerThreadRunnable task : completionstatus){
			if(!task.isExecutionDone()){
				result = false;
				break;
			}
		}
		setDone(result);
	}
	
	/**
	 * Responsible to execute additional tasks as soon as the execution of the list 
	 * of tasks in {@link #executeListOfTasks()} is effectively done. 
	 * Currently it makes sure in the parallel case. 
	 * @param callerid the id of the caller for log purposes.
	 */
	public void finalizeTaskExecution(String callerid){
		Thread completionTask = new Thread("CompletionThreadFOR"+callerid){
			public void run() {
				while(true){
					// Check the task state each 2 seconds
					try {
						Thread.sleep(2000);
					} catch (InterruptedException e) {
						log.error("finalizeTaskExecution from ["+ schedulerid + "] ", e);
					}
					if(isDone()){
						// logic to be added here for additional tasks 
						// to be executed at the end of all tasks
						break;
					}else{
						updateTaskCompletionState();
					}
				}
				log.info("END - Parallel execution of "+callerid);
			}
		};
		completionTask.setDaemon(true);
		completionTask.start();
	}
}
