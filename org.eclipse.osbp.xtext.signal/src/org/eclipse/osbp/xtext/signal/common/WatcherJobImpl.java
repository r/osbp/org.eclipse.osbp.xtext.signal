package org.eclipse.osbp.xtext.signal.common;

import java.nio.file.Path;
import java.util.HashSet;

import org.eclipse.osbp.xtext.datainterchange.common.WorkerThreadRunnable;

public class WatcherJobImpl extends SignalCommonData {
	/** the watcher full qualified name */
	private String watcherid;
	/** the trigger file*/
	private Path triggerfile;
	/** whether the job can be executed in parallel*/
	private boolean seqjobexec;
	/** the task status*/
	private boolean done;
	/** holds the completion status of each task in the parallel case*/
	private HashSet<WorkerThreadRunnable> completionstatus;
	
	/**
	 * Default constructor
	 */
	public WatcherJobImpl(){
		completionstatus = new HashSet<>();
	}
	
	/**
	 * Triggers the execution of the list of task.
	 * This operation has to be overridden by subclasses.
	 */
	public void executeListOfTasks(){}

	/**
	 * Returns the {@link #triggerfile}.
	 * @return {@link Path} the trigger file.
	 */
	public Path getTriggerfile() {
		return triggerfile;
	}

	/**
	 * Sets the trigger file {@link #triggerfile}.
	 * @param triggerfile the new value
	 */
	public void setTriggerfile(Path triggerfile) {
		this.triggerfile = triggerfile;
	}

	/**
	 * Returns the watcher full qualifier id.
	 * @return {@link String} the value of {@link #watcherid}
	 */
	public String getWatcherId() {
		return watcherid;
	}

	/**
	 * Sets the watcher id value {@link #watcherid}.
	 * @param watcherid the new value
	 */
	public void setWatcherId(String watcherid) {
		this.watcherid = watcherid;
	}
	
	/**
	 * Returns the value of {@link #seqjobexec}, which determines
	 * wether a watcher job can be executed in parallel fashion.
	 * @return {@link Boolean} true if yes, false if not.
	 */
	public boolean isParallelJobExecutionAllowed() {
		return seqjobexec;
	}

	/**
	 * Sets the state of {@link #seqjobexec}.
	 * @param noseq the {@link #seqjobexec} value
	 */
	public void setParallelJobExecutionAllowed(boolean noseq) {
		this.seqjobexec = noseq;
	}

	/**
	 * Indicates if the execution of the list of tasks is done or not.
	 * @return true if yes, false if not.
	 */
	public boolean isDone() {
		return done;
	}

	/**
	 * Sets the state of watcher task: done or not.
	 * @param done the state value
	 */
	public void setDone(boolean done) {
		this.done = done;
	}

	/**
	 * Adds an (interchange) task to the list of all tasks,
	 * in order to monitor when it's done.
	 * @param task the {@link WorkerThreadRunnable} task
	 */
	public void checkForCompletion(WorkerThreadRunnable task) {
		completionstatus.add(task);
	}
	
	/**
	 * Updates the state of this watcher task.
	 */
	public void updateTaskCompletionState(){
		boolean result = true;
		for(WorkerThreadRunnable task : completionstatus){
			if(!task.isExecutionDone()){
				result = false;
				break;
			}
		}
		setDone(result);
	}
	
	/**
	 * Responsible to execute additional tasks as soon as the execution of the list 
	 * of tasks in {@link #executeListOfTasks()} is effectively done. 
	 * Currently it handles the deletion of the trigger file in the parallel case. 
	 * @param callerid the id of the caller for log purposes.
	 */
	public void finalizeTaskExecution(String callerid){
		Thread completionTask = new Thread("CompletionThreadFOR"+callerid){
			public void run() {
				while(true){
					// Check the task state each 2 seconds
					try {
						Thread.sleep(2000);
					} catch (InterruptedException e) {
						log.error("finalizeTaskExecution from ["+ watcherid + "] ", e);
					}
					if(isDone()){
						deleteFile(getTriggerfile(), callerid+" - Triggerfile");
						break;
					}else{
						updateTaskCompletionState();
					}
				}
				log.info("WatcherJobsHandler - END - Parallel execution of "+callerid);
			}
		};
		completionTask.setDaemon(true);
		completionTask.start();
	}
}
