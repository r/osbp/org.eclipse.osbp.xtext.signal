/**
 *                                                                            
 *  Copyright (c) 2011, 2018 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
package org.eclipse.osbp.xtext.signal.common;

import static org.quartz.CronScheduleBuilder.cronSchedule;
import static org.quartz.CronScheduleBuilder.dailyAtHourAndMinute;
import static org.quartz.CronScheduleBuilder.monthlyOnDayAndHourAndMinute;
import static org.quartz.CronScheduleBuilder.weeklyOnDayAndHourAndMinute;
import static org.quartz.JobBuilder.newJob;
import static org.quartz.TriggerBuilder.newTrigger;

import java.util.Properties;
import java.util.concurrent.ExecutorService;

import org.quartz.CronExpression;
import org.quartz.CronTrigger;
import org.quartz.DateBuilder;
import org.quartz.Job;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.impl.StdSchedulerFactory;

public class SchedulerImpl extends SignalCommonData {
	
	/** the executor services for the processing of all tasks */
	private static ExecutorService executorService;
	
	public SchedulerImpl(){
		signalcount++;
	}
	
	/**
	 * Initializes the {@link #scheduler} field.
	 * 
	 * @param schedname
	 *            the scheduler name
	 * @throws SchedulerException
	 *             possible thrown exception
	 */
	public Scheduler createScheduler(String schedname) throws SchedulerException {
		log.info("createScheduler " + schedname + " ...");
		Scheduler scheduler = null;
		try {
			Properties properties = new Properties();
			properties.put(StdSchedulerFactory.PROP_SCHED_INSTANCE_ID, "AUTO");
			properties.put(StdSchedulerFactory.PROP_SCHED_INSTANCE_NAME, schedname);
			properties.put(StdSchedulerFactory.PROP_SCHED_NAME, schedname);
			properties.put(StdSchedulerFactory.PROP_THREAD_POOL_PREFIX + ".threadCount", "1");
			properties.put(StdSchedulerFactory.PROP_THREAD_POOL_CLASS, "org.quartz.simpl.SimpleThreadPool");
			StdSchedulerFactory factory = new StdSchedulerFactory(properties);
			scheduler = factory.getScheduler();
		} catch (ArrayIndexOutOfBoundsException e) {
			log.debug(e.getMessage());
		}
		log.info(schedname + " successfully created and initialized!");
		return scheduler;
	}

	
	/**
	 * *Creates a job based on the given watcher - handler name.
	 * 
	 * @param name
	 *            Job name
	 * @param groupname
	 *            Group name
	 * @param description
	 *            Description of the job
	 * @param issequential
	 *            Execution type of the given lsit of tasks
	 * @param handlername
	 *            name of watcher - handler for this specific job.
	 * @return {@link JobDetail} the newly created Job.
	 */
	public JobDetail createDataTransferJob(String name, String groupname, String description, boolean issequential,
			String handlername, Class<? extends Job> jobClass) {
		JobDetail job = newJob(jobClass).usingJobData(SignalConstants.SEQUENTIAL, issequential)
				.withDescription(description).withIdentity(name, groupname).build();
		job.getJobDataMap().putIfAbsent(SignalConstants.TASKSLIST, handlername);
		return job;
	}

	/**
	 * Creates an hourly based trigger.
	 * 
	 * @param name
	 *            Trigger name
	 * @param groupname
	 *            Group name
	 * @param minute
	 *            The exact minute, in which the trigger is hourly fired (0-59).
	 * @return {@link CronTrigger} the newly created trigger.
	 */
	public CronTrigger createHourlyTrigger(String name, String groupname, int minute) {
		log.debug("Creating createHourlyTrigger [" + name + "] for group [" + groupname + "] running hourly at ["
				+ minute + "] minutes ...");

		CronTrigger trigger = newTrigger().withIdentity(name, groupname)
				.withSchedule(cronSchedule(String.format("0 %s * * * ?", minute))).build();

		log.debug("createHourlyTrigger " + trigger + " successfully created.");
		return trigger;
	}

	/**
	 * Creates a daily based trigger.
	 * 
	 * @param name
	 *            Trigger name
	 * @param groupname
	 *            Group name
	 * @param hour
	 *            The exact hour, in which the trigger is daily fired (0-23).
	 * @param minute
	 *            The exact minute, in which the trigger is hourly in a day
	 *            fired (0-59).
	 * @return {@link CronTrigger} the newly created trigger.
	 */
	public CronTrigger createDailyTriggerAtHourandMins(String name, String groupname, int hour, int minute) {
		log.debug("Creating DailyTrigger [" + name + "] for group [" + groupname + "] running at hour[" + hour
				+ "] and [" + minute + "] minutes ...");
		CronTrigger trigger = newTrigger().withIdentity(name, groupname)
				.withSchedule(dailyAtHourAndMinute(hour, minute)).build();
		log.debug("DailyTrigger " + trigger + " successfully created.");
		return trigger;
	}

	/**
	 * Creates a weekly based trigger.
	 * 
	 * @param name
	 *            Trigger name
	 * @param groupname
	 *            Group name
	 * @param dayOfWeek
	 *            The day of the week on which the trigger has to be fired (0-6
	 *            for Sunday-Saturday).
	 * @param hour
	 *            The exact hour, in which the trigger is daily fired (0-23).
	 * @param minute
	 *            The exact minute, in which the trigger is hourly in a day
	 *            fired (0-59).
	 * @return {@link CronTrigger} the newly created trigger.
	 */
	public CronTrigger createWeeklyTriggerOnDayAndHourAndMinute(String name, String groupname, int dayOfWeek, int hour,
			int minute) {
		log.debug("Creating Trigger [" + name + "] for group [" + groupname + "] ...");
		CronTrigger trigger = newTrigger().withIdentity(name, groupname)
				.withSchedule(weeklyOnDayAndHourAndMinute(dayOfWeek, hour, minute)).build();
		log.debug("Trigger " + trigger + " successfully created.");
		return trigger;
	}

	/**
	 * Creates a monthly based trigger.
	 * 
	 * @param name
	 *            Trigger name
	 * @param groupname
	 *            Group name
	 * @param dayOfMonth
	 *            The day of the month on which the trigger has to be fired
	 *            (1-31).
	 * @param hour
	 *            The exact hour, in which the trigger is daily fired (0-23).
	 * @param minute
	 *            The exact minute, in which the trigger is hourly in a day
	 *            fired (0-59).
	 * @return {@link CronTrigger} the newly created trigger.
	 */
	public CronTrigger createMonthlyTriggerOnDayAndHourAndMinute(String name, String groupname, int dayOfMonth,
			int hour, int minute) {
		log.debug("Creating MonthlyTrigger[" + name + "] for group [" + groupname + "] on Day[" + dayOfMonth
				+ "] and Hour[" + hour + "] and [" + minute + "]Minute...");
		CronTrigger trigger = newTrigger().withIdentity(name, groupname)
				.withSchedule(monthlyOnDayAndHourAndMinute(dayOfMonth, hour, minute)).build();
		log.debug("MonthlyTrigger " + trigger + " successfully created.");
		return trigger;
	}

	/**
	 * Creates a cron based trigger, depending on the given cron-expression.
	 * 
	 * @param name
	 *            Trigger name
	 * @param groupname
	 *            Group name
	 * @param expression
	 *            the {@link CronExpression} as string value
	 * @return {@link CronTrigger} the newly created cron-trigger.
	 */
	public CronTrigger createCronTrigger(String name, String groupname, String expression) {
		log.debug("Creating CronTrigger [" + name + "] for group [" + groupname + "] with CronExpression [" + expression
				+ "]...");
		CronTrigger trigger = newTrigger().withIdentity(name, groupname).withSchedule(cronSchedule(expression)).build();
		log.debug("CronTrigger " + trigger + " successfully created.");
		return trigger;
	}

	/**
	 * Checks if the given cron expression is valid and gives it back
	 * eventually.
	 * 
	 * @param expression
	 *            the cron expression
	 * @return a valid cron expression or null
	 */
	public String getCronExpressionValue(String expression) {
		if (expression != null && CronExpression.isValidExpression(expression)) {
			return expression;
		}
		return null;
	}

	/**
	 * Checks if the given minutes value is between 0 and 59 and gives the
	 * appropriate integer value back.
	 * 
	 * @param minutes
	 *            the minutes
	 * @return a value between 0 and 59, or -1
	 */
	public int getMinutesValue(String minutes) {
		try {
			int value = Integer.valueOf(minutes);
			if (value >= 0 && value <= 59) {
				return value;
			}
		} catch (NumberFormatException e) {
			log.error("getMinutesValue - given minute value [" + minutes + "] is not valid.");
		}
		return -1;
	}

	/**
	 * Checks if the given hour value is between 0 and 23 and gives the
	 * appropriate integer value back.
	 * 
	 * @param hour
	 *            the hour
	 * @return a value between 0 and 23, or -1
	 */
	public int getHourValue(String hour) {
		try {
			int value = Integer.valueOf(hour);
			if (value >= 0 && value <= 23) {
				return value;
			}
		} catch (NumberFormatException e) {
			log.error("getHourValue - given hour value [" + hour + "] is not valid.");
		}
		return -1;
	}

	/**
	 * Checks if the given day of the week value is valid and give the
	 * appropriate integer value back. Valid value here are: sunday | monday |
	 * tuesday | wednesday | thursday | friday | saturday
	 * 
	 * @param dayofweek
	 *            the day of the week
	 * @return a value between 1 and 7, or -1
	 */
	public int getDayOfWeekValue(String dayofweek) {
		switch (dayofweek.toLowerCase()) {
		case "sunday":
			return DateBuilder.SUNDAY;
		case "monday":
			return DateBuilder.MONDAY;
		case "tuesday":
			return DateBuilder.TUESDAY;
		case "wednesday":
			return DateBuilder.WEDNESDAY;
		case "thursday":
			return DateBuilder.THURSDAY;
		case "friday":
			return DateBuilder.FRIDAY;
		case "saturday":
			return DateBuilder.SATURDAY;
		default:
			log.error("getDayOfWeekValue - given dayofweek value [" + dayofweek + "] is not valid.");
			return -1;
		}
	}

	/**
	 * Checks if the given day of the month value is between 1 and 31 and gives
	 * the appropriate integer value back.
	 * 
	 * @param dayofmonth
	 *            the day of month
	 * @return a value between 1 and 31, or -1
	 */
	public int getDayOfMonthValue(String dayofmonth) {
		try {
			int value = Integer.valueOf(dayofmonth);
			if (value >= 1 && value <= 31) {
				return value;
			}
		} catch (NumberFormatException e) {
			log.error("getDayofMonthValue - given dayofmonth value [" + dayofmonth + "] is not valid.");
		}
		return -1;
	}

	/**
	 * Returns the {@link #executorService}.
	 * @return {@link ExecutorService} the executor service.
	 */
	public static ExecutorService getExecutorService() {
		return executorService;
	}

	/**
	 * Initialise the {@link #executorService}.
	 * @param executorname the name of the service
	 */
	public void setExecutorService(String executorname) {
		while(true){
			if(getMaxParallelThreadsCount() == -1){
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					// NOSONAR
				}				
			}
			else{
				executorService = createCustomExecutorService(executorname, getMaxParallelThreadsCount(), 0L);
				break;
			}
		}
	}

}
