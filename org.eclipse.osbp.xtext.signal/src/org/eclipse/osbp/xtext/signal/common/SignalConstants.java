/**
 *                                                                            
 *  Copyright (c) 2011, 2018 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
package org.eclipse.osbp.xtext.signal.common;

public abstract class SignalConstants {

	/** Task execution types */
	public static final String SEQUENTIAL = "sequential";
	public static final String TASKSLIST = "taskslist";

	/** Information notice on cron expression usage. */
	public static final String CRONEXP_NOTICE = "\n\nFurther information about how to properly build the cron-expression can be found here: "
	+ "\nhttp://www.quartz-scheduler.org/documentation/quartz-2.2.x/tutorials/tutorial-lesson-06.html"
	+ "\n\nHere are some full examples: "
	+ "\nExpression						Meaning"
	+"\n\"0 0 12 * * ?\"    				Fire at 12pm (noon) every day" 
	+"\n\"0 15 10 ? * *\"    				Fire at 10:15am every day "
	+"\n\"0 15 10 * * ?\"  					Fire at 10:15am every day "
	+"\n\"0 15 10 * * ? *\"    				Fire at 10:15am every day "
	+"\n\"0 15 10 * * ? 2005\"    			Fire at 10:15am every day during the year 2005  "
	+"\n\"0 * 14 * * ?\"    				Fire every minute starting at 2pm and ending at 2:59pm, every day  "
	+"\n\"0 0/5 14 * * ?\"    				Fire every 5 minutes starting at 2pm and ending at 2:55pm, every day  "
	+"\n\"0 0/5 14,18 * * ?\"    			Fire every 5 minutes starting at 2pm and ending at 2:55pm, AND fire every 5 minutes starting at 6pm and ending at 6:55pm, every day"  
	+"\n\"0 0-5 14 * * ?\"    				Fire every minute starting at 2pm and ending at 2:05pm, every day  "
	+"\n\"0 10,44 14 ? 3 WED\"    			Fire at 2:10pm and at 2:44pm every Wednesday in the month of March.  "
	+"\n\"0 15 10 ? * MON-FRI\"   			Fire at 10:15am every Monday, Tuesday, Wednesday, Thursday and Friday"  
	+"\n\"0 15 10 15 * ?\"    				Fire at 10:15am on the 15th day of every month  "
	+"\n\"0 15 10 L * ?\"    				Fire at 10:15am on the last day of every month  "
	+"\n\"0 15 10 ? * 6L\"    				Fire at 10:15am on the last Friday of every month  "
	+"\n\"0 15 10 ? * 6L\"    				Fire at 10:15am on the last Friday of every month  "
	+"\n\"0 15 10 ? * 6L 2002-2005\"    	Fire at 10:15am on every last Friday of every month during the years 2002, 2003, 2004 and 2005"  
	+"\n\"0 15 10 ? * 6#3\"    				Fire at 10:15am on the third Friday of every month  "

	+"\n\nPay attention to the effects of '?' and '*' in the day-of-week and day-of-month fields!" 
	
	+"\n\nNOTES:" 
	+"\nSupport for specifying both a day-of-week and a day-of-month value is not complete (you'll need to use the '?' character in on of these fields)." 
	+"\nBe careful when setting fire times between mid-night and 1:00 AM - \"daylight savings\" can cause a skip or a repeat depending on whether the time moves back or jumps forward.";
	
	/* Scheduler/Trigger constants for configuration file */
	public static final String DIRECTORY = ".directory";
	public static final String FILEMASK = ".filemask";
	public static final String FILENAME = ".filename";
	public static final String CRON_SCHEDULER = ".cronscheduler";
	public static final String HOURLY_SCHEDULER = ".hourlyscheduler.minute";
	public static final String DAILY_SCHEDULER_MIN = ".dailyscheduler.minute";
	public static final String DAILY_SCHEDULER_HOUR = ".dailyscheduler.hour";
	public static final String WEEKLY_SCHEDULER_HOUR = ".weeklyscheduler.hour";
	public static final String WEEKLY_SCHEDULER_MIN = ".weeklyscheduler.minute";
	public static final String WEEKLY_SCHEDULER_DAYOFWEEK = ".weeklyscheduler.dayofweek";
	public static final String MONTHLY_SCHEDULER_HOUR = ".monthlyscheduler.hour";
	public static final String MONTHLY_SCHEDULER_MIN = ".monthlyscheduler.minute";
	public static final String MONTHLY_SCHEDULER_DAYOFMONTH = ".monthlyscheduler.dayofmonth";
	/** maximum thread count property name for configuration files*/
	public static final String MAXPARALLELTHREADCOUNT_NAME = "Max-Parallel-Threads-Count";
	/** maximum thread count for parallel job execution*/
	public static final int MAXPARALLELTHREADCOUNT = 15;
	/** default thread count for parallel job execution*/
	public static final int DEFAULT_PARALLEL_THREADCOUNT = 5;
	/** time to wait for watcher to init*/
	public static final long WAITINGTIME_FOR_WATCHER_INIT = 2000L;
	/** time to wait for job completion*/
	public static final long WAITINGTIME_FOR_WATCHER_CHECK = 2000L;
	/** time to wait for job completion*/
	public static final long WAITINGTIME_FOR_JOBCOMPLETION_CHECK = 2000L;
	/** time to wait before checking if there are job to handle in the watcher queue*/
	public static final long WAITINGTIME_FOR_NEWJOB_CHECK = 2000L;
	
	public static final String CSV_EXTENSION = ".csv";
	public static final String XML_EXTENSION = ".xml";
	public static final String EDI_EXTENSION = ".edi";
}
