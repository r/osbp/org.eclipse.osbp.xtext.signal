/**
 *                                                                            
 *  Copyright (c) 2011, 2018 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 *
 */
package org.eclipse.osbp.xtext.signal.imports;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.osbp.xtext.datainterchange.DataDSLPackage;
import org.eclipse.osbp.xtext.datainterchange.DataInterchange;
import org.eclipse.osbp.xtext.functionlibrarydsl.FunctionLibraryActionGroup;
import org.eclipse.osbp.xtext.functionlibrarydsl.FunctionLibraryDSLPackage;
import org.eclipse.osbp.xtext.functionlibrarydsl.FunctionLibraryExecute;
import org.eclipse.osbp.xtext.oxtype.imports.DefaultShouldImportProvider;

public class ShouldImportProvider extends DefaultShouldImportProvider {

	protected boolean doShouldImport(EObject toImport, EReference eRef, EObject context) {
		return toImport instanceof DataInterchange 
		|| toImport instanceof FunctionLibraryActionGroup
		|| toImport instanceof FunctionLibraryExecute;
	}

	protected boolean doShouldProposeAllElements(EObject object, EReference reference) {
		EClass type = reference.getEReferenceType();
		return DataDSLPackage.Literals.DATA_INTERCHANGE.isSuperTypeOf(type) 
		|| FunctionLibraryDSLPackage.Literals.FUNCTION_LIBRARY_ACTION_GROUP.isSuperTypeOf(type)
		|| FunctionLibraryDSLPackage.Literals.FUNCTION_LIBRARY_EXECUTE.isSuperTypeOf(type);
	}
}
