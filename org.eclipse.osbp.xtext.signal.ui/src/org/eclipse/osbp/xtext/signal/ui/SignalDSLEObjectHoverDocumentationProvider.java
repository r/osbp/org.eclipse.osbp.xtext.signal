/**
 *                                                                            
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *                                                                            
 * All rights reserved. This program and the accompanying materials           
 * are made available under the terms of the Eclipse Public License 2.0        
 * which accompanies this distribution, and is available at                  
 * https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 * SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 * Contributors:   
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation 
 */
package org.eclipse.osbp.xtext.signal.ui;

import org.eclipse.osbp.xtext.basic.ui.BasicDSLDocumentationTranslator;
import org.eclipse.osbp.xtext.basic.ui.BasicDSLEObjectHoverDocumentationProvider;

public class SignalDSLEObjectHoverDocumentationProvider extends BasicDSLEObjectHoverDocumentationProvider {
	 
    private static SignalDSLEObjectHoverDocumentationProvider INSTANCE;

    public static SignalDSLEObjectHoverDocumentationProvider instance() {
        return INSTANCE;
    }

    public SignalDSLEObjectHoverDocumentationProvider() {
        super();
        INSTANCE = this;
    }
    
    @Override
    protected BasicDSLDocumentationTranslator getTranslator() {
        return SignalDSLDocumentationTranslator.instance();
    }
}
